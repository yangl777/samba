%bcond_with testsuite
%bcond_without clustering
%{?python3_sitearch: %global python3_sitearch %{python3_sitearch}}

%global talloc_version 2.4.3
%global tdb_version 1.4.13
%global tevent_version 0.16.2

%undefine _strict_symbol_defs_build

%global with_libsmbclient 1
%global with_libwbclient 1
%global with_profiling 1
%global with_vfs_cephfs 0

%global with_vfs_glusterfs 0
%ifarch x86_64
%global with_vfs_glusterfs 1
%endif

%global with_dc 1

%if %{with testsuite}
%global with_dc 1
%endif

%global required_mit_krb5 1.20.1

%global with_clustering_support 0

%if %{with clustering}
%global with_clustering_support 1
%endif

%bcond_with ccache

%global with_winexe 0

%ifnarch ppc64le
%global with_vfs_io_uring 0
%endif

%global _systemd_extra "Environment=KRB5CCNAME=FILE:/run/samba/krb5cc_samba"
%global samba_depver %{version}-%{release}

Name:           samba
Version:        4.22.0
Release:        1

Summary:        A suite for Linux to interoperate with Windows
License:        GPL-3.0-or-later AND LGPL-3.0-or-later
URL:            https://www.samba.org

Source0:        https://download.samba.org/pub/samba/stable/%{name}-%{version}.tar.gz
Source1:        https://download.samba.org/pub/samba/stable/%{name}-%{version}.tar.asc
Source2:        samba-pubkey_AA99442FB680B620.gpg
Source3:        samba.logrotate
Source4:        smb.conf.vendor
Source5:        smb.conf.example
Source6:        pam_winbind.conf
Source7:        samba.pamd
Source8:        usershares.conf.vendor
Source9:        samba-systemd-sysusers.conf
Source10:       samba-usershares-systemd-sysusers.conf
Source11:       samba-winbind-systemd-sysusers.conf

Source201:      README.downgrade

Patch1:         remove-sensitive-info.patch


BuildRequires: avahi-devel bison dbus-devel docbook-style-xsl e2fsprogs-devel flex gawk gnupg2 gnutls-devel >= 3.4.7 gpgme-devel
BuildRequires: jansson-devel krb5-devel >= %{required_mit_krb5} libacl-devel libaio-devel libarchive-devel libattr-devel 
BuildRequires: libcap-devel libicu-devel libcmocka-devel libtirpc-devel libuuid-devel libxslt lmdb ncurses-devel openldap-devel
BuildRequires: pam-devel perl-interpreter perl-generators perl(Archive::Tar) perl(Test::More) popt-devel python3-devel python3-setuptools quota-devel
BuildRequires: readline-devel rpcgen rpcsvc-proto-devel sed libtasn1-devel libtasn1-tools xfsprogs-devel xz zlib-devel >= 1.2.3 python3-dns python3-requests python3-cryptography
BuildRequires: gcc make libxcrypt-devel
BuildRequires: chrpath
# buildrequires for libldb
BuildRequires: doxygen lmdb-devel >= 0.9.16

%if %{?openEuler:1}0
BuildRequires: cups-devel
%endif

%if %{with_winexe}
BuildRequires: mingw32-gcc
BuildRequires: mingw64-gcc
%endif

BuildRequires: pkgconfig(libsystemd)

%if %{with_vfs_glusterfs}
BuildRequires: glusterfs-api-devel >= 3.4.0.16 glusterfs-devel >= 3.4.0.16  libgfapi0
%endif

%if %{with_vfs_cephfs}
BuildRequires: libcephfs-devel
%endif

%if %{with_dc}
BuildRequires: bind krb5-server >= %{required_mit_krb5} python3-pyasn1 >= 0.4.8
%endif

BuildRequires: perl(ExtUtils::MakeMaker) perl(FindBin) perl(Parse::Yapp) libtalloc-devel >= %{talloc_version} python3-talloc-devel >= %{talloc_version}
BuildRequires: libtevent-devel >= %{tevent_version} python3-tevent >= %{tevent_version}

BuildRequires: libtdb-devel >= %{tdb_version} python3-tdb >= %{tdb_version}

%if %{with testsuite} || %{with_dc}
BuildRequires: ldb-tools tdb-tools python3-dateutil python3-gpgme python3-markdown
%endif

%if %{with_vfs_io_uring}
BuildRequires: liburing-devel >= 0.4
%endif

Requires:       systemd shadow-utils pam %{name}-common = %{samba_depver}
Requires:       %{name}-common = %{samba_depver} %{name}-common-tools = %{samba_depver}
Requires:       %{name}-client-libs = %{samba_depver}
Requires:       libldb = %{samba_depver}
%if %with_libwbclient
Requires:       libwbclient = %{samba_depver}
%endif

Provides:       samba4 = %{samba_depver} samba-doc = %{samba_depver} samba-domainjoin-gui = %{samba_depver}
Provides:       samba-swat = %{samba_depver} samba4-swat = %{samba_depver}
Obsoletes:      samba4 < %{samba_depver} samba-doc < %{samba_depver} samba-domainjoin-gui < %{samba_depver}
Obsoletes:      samba-swat < %{samba_depver} samba4-swat < %{samba_depver}

%description
Samba is a suite of programs for Linux and Unix to interoperate with Windows.

%package libs
Summary:        Libraries for %{name}
Requires:       %{name}-common-libs = %{samba_depver}
Requires:       %{name}-client-libs = %{samba_depver}
Requires:       libldb = %{samba_depver}
%if %with_libwbclient
Requires:       libwbclient = %{samba_depver}
%endif

Provides:       samba4-libs = %{samba_depver}
Obsoletes:      samba4-libs < %{samba_depver}
Obsoletes:      python2-samba-test < %{samba_depver}
Obsoletes:      samba-test-libs < %{samba_depver}

%description libs
Librariesfor%{name}.

%package client
Summary:        Client package for %{name}
Requires: 		%{name}-common = %{samba_depver}
Requires: 		%{name}-client-libs = %{samba_depver}
Requires:       chkconfig
Requires:       libldb = %{samba_depver}
%if %with_libsmbclient
Requires: libsmbclient = %{samba_depver}
%endif
%if %with_libwbclient
Requires: libwbclient = %{samba_depver}
%endif

Provides: samba4-client = %{samba_depver}
Obsoletes: samba4-client < %{samba_depver}
Obsoletes: python2-samba 

%description client
This package includes some files about SMB/CIFS clients to complement the SMB/CIFS clients.

%package client-libs
Summary: Samba client libraries
Requires: %{name}-common = %{samba_depver}
Requires: libldb = %{samba_depver}
%if %with_libwbclient
Requires: libwbclient = %{samba_depver}
%endif
Requires: krb5-libs >= %{required_mit_krb5}

%description client-libs
The samba-client-libs package contains internal libraries needed by the SMB/CIFS clients.

%package common
Summary:        Common package for %{name} client and server
Requires: 		systemd
Recommends:     logrotate

%if ! %{with_dc}
Obsoletes: samba-dc < %{samba_depver}
Obsoletes: samba-dc-libs < %{samba_depver}
Obsoletes: samba-dc-bind-dlz < %{samba_depver}
%endif

Requires: libldb = %{samba_depver}
Requires: %{name}-client-libs = %{samba_depver}
%if %with_libwbclient
Requires: libwbclient = %{samba_depver}
%endif
Provides:       samba4-common = %{samba_depver} %{name}-common-libs = %{samba_depver}
Obsoletes:      samba4-common < %{samba_depver} %{name}-common-libs < %{samba_depver}


%description common
This package contains some common basic files needed by %{name} client
and server.

# ctdb-tests package has been dropped if we do not build the testsuite
%if %with_clustering_support
%if %{without testsuite}
Obsoletes: ctdb-tests < %{samba_depver}
# endif without testsuite
%endif
# endif with clustering support
%endif

%package common-tools
Summary:        Tools package for %{name}
Requires:       %{name}-common = %{samba_depver} %{name}-client-libs = %{samba_depver}
Requires:       %{name}-libs = %{samba_depver}
Requires:       libldb = %{samba_depver}
%if %with_libwbclient
Requires:       libwbclient = %{samba_depver}
%endif

%description common-tools
This package contains some tools for %{name} server and client.

%package tools
Summary:        Tools for Samba servers
Requires:       python3-%{name} = %{samba_depver} python3-%{name}-dc = %{samba_depver}
%if %{with dc}
Requires:       lmdb tdb-tools python3-gpgme
%endif

%description tools
The samba-tools package contains tools for Samba servers and for GPO management 
on domain members.

%package dc
Summary:        Domain Controller package for %{name}
Requires: 		%{name} = %{samba_depver} %{name}-winbind = %{samba_depver}
Requires:       %{name}-common = %{samba_depver} tdb-tools
Requires: 		%{name}-libs = %{samba_depver}
Requires:       %{name}-dc-provision = %{samba_depver}
Requires:       %{name}-dc-libs = %{samba_depver}
Requires: 		lmdb ldb-tools
Requires:       libldb = %{samba_depver}
Requires:       libwbclient = %{samba_depver}

Provides:       samba4-dc = %{samba_depver}
Obsoletes:      samba4-dc < %{samba_depver}

%description dc
The samba-dc package provides AD Domain Controller functionality, including some
libraries.


%package dc-provision
Summary: Samba AD files to provision a DC

%description dc-provision
The samba-dc-provision package provides files to setup a domain controller

%package dc-libs
Summary:        Samba AD Domain Controller Libraries
Requires:       %{name}-client-libs = %{samba_depver}
Requires:       %{name}-common = %{samba_depver}
Requires:       %{name}-libs = %{samba_depver}
Requires:       libwbclient = %{samba_depver}
 
Provides:       samba4-dc-libs = %{samba_depver}
Obsoletes:      samba4-dc-libs < %{samba_depver}
 
%description dc-libs
The %{name}-dc-libs package contains the libraries needed by the DC to
link against the SMB, RPC and other protocols.

%package dc-bind-dlz
Summary: Bind DLZ module for Samba AD
Requires: %{name}-common = %{samba_depver} %{name}-dc = %{samba_depver} bind
Requires: libldb = %{samba_depver}
Requires: libwbclient = %{samba_depver}

%description dc-bind-dlz
This package contains the library files to manage name server related details of
Samba AD.

%package devel
Summary: Developer tools for Samba libraries
Requires: %{name}-libs = %{samba_depver} %{name}-client-libs = %{samba_depver}
Requires: %{name}-dc-libs = %{samba_depver}

Provides: samba4-devel = %{samba_depver}
Obsoletes: samba4-devel < %{samba_depver}
Provides:  python3-samba-devel = %{samba_depver}
Obsoletes: python3-samba-devel < %{samba_depver}

%description devel
This package contains some header files and library files for %{name}.

%if %{with_vfs_cephfs}
%package vfs-cephfs
Summary:        The VFS module for Ceph distributed storage system
Requires:       %{name} = %{samba_depver}
Requires:       %{name}-libs = %{samba_depver}
Requires:       libldb = %{samba_depver}
Requires:       libwbclient = %{samba_depver}


%description vfs-cephfs
This is the samba VFS module for Ceph distributed storage system integration.
#endif with_vfs_cephfs
%endif

### GLUSTER
%if %{with_vfs_glusterfs}
%package vfs-glusterfs
Summary: 		Samba VFS module for GlusterFS
Requires:       glusterfs-api >= 3.4.0.16 glusterfs >= 3.4.0.16
Requires:       %{name} = %{samba_depver} %{name}-libs = %{samba_depver}
Requires:       %{name}-common = %{samba_depver} %{name}-client-libs = %{samba_depver}
Requires:       libldb = %{samba_depver}

%if %with_libwbclient
Requires: libwbclient = %{samba_depver}
%endif

Obsoletes: samba-glusterfs < %{samba_depver}
Provides: samba-glusterfs = %{samba_depver}

%description vfs-glusterfs
Samba VFS module for GlusterFS integration.
%endif

%package krb5-printing
Summary:        The samba CUPS backend package for printing with Kerberos
Requires: 	%{name}-client = %{samba_depver}
Requires:       %{name}-client-libs = %{samba_depver}

%description krb5-printing
This package will allow cups to access the Kerberos credentials cache
of the user issuing the print job.


%if %with_libsmbclient
%package -n libsmbclient
Summary: 		The SMB client library
Requires: 		%{name}-common = %{samba_depver} %{name}-client-libs = %{samba_depver}
Requires:		libldb = %{samba_depver}
%if %with_libwbclient
Requires: libwbclient = %{samba_depver}
%endif
Obsoletes: 		python2-samba

%description -n libsmbclient
This pacakge contains the SMB client library from the Samba suite.

%package -n libsmbclient-devel
Summary:        Development package for the SMB client library
Requires: 		libsmbclient = %{samba_depver}

%description -n libsmbclient-devel
This package provides developer tools for the wbclient library.
#endif with_libsmbclient
%endif

%if %with_libwbclient
%package -n libwbclient
Summary:        The winbind client library for %{name}
Requires:       %{name}-client-libs = %{samba_depver}
Conflicts:	sssd-libwbclient < 2.5.1

%description -n libwbclient
This package contains the winbind client library from the Samba.

%package -n libwbclient-devel
Summary:        The development package for the winbind library
Requires:       libwbclient = %{samba_depver}
Provides:       samba-winbind-devel = %{samba_depver}
Obsoletes:      samba-winbind-devel < %{samba_depver}
Conflicts:	sssd-libwbclient-devel < 2.5.1

%description -n libwbclient-devel
This package provides developer tools for the wbclient library.
#endif with_libwbclient
%endif

### PYTHON3
%package -n python3-%{name}
Summary:        Python3 library package for %{name}
Requires:       %{name}-client-libs = %{samba_depver} %{name}-common = %{samba_depver}
Requires:       python3-talloc python3-tevent python3-tdb python3-ldb python3-dns python3-requests python3-cryptography
Requires:       %{name}-libs = %{samba_depver}
Requires:       libldb = %{samba_depver}
Obsoletes:	python2-samba
Obsoletes:      python3-samba < %{samba_depver}

%if %with_libsmbclient
Requires: libsmbclient = %{samba_depver}
%endif
%if %with_libwbclient
Requires: libwbclient = %{samba_depver}
%endif
%if %{with dc}
Requires: %{name}-dc-libs = %{samba_depver}
%endif

%description -n python3-%{name}
This package contains the Python 3 libraries needed by programs
that use SMB, RPC and other Samba provided protocols in Python 3 programs.

%package -n python3-samba-test
Summary:        Test package for python3 binding for %{name}

Requires:       python3-%{name} = %{samba_depver}
Requires: 		%{name}-client-libs = %{samba_depver}
Requires: 		%{name}-libs = %{samba_depver}
Obsoletes:      python3-samba-test < %{samba_depver}

%description -n python3-samba-test
This package contains the Python libraries used by the test suite of Samba.
If you want to run full set of Samba tests, you need to install this package.

%if %{with_dc}
%package -n python3-samba-dc
Summary:        The Samba Python libraries for Samba AD
Requires:       python3-%{name} = %{samba_depver}
Requires:       %{name}-dc-libs = %{samba_depver}
Requires: 	python3-markdown
Requires: 	libldb = %{samba_depver}
Requires:       libwbclient = %{samba_depver}

%description -n python3-samba-dc
This contains the Python libraries needed by programs
to manage Samba AD.
%endif

%package pidl
Summary:        Perl IDL compiler package for %{name}
Requires:       perl(Parse::Yapp)
Requires: perl-interpreter
Requires: perl(FindBin)
BuildArch:      noarch
Provides:       samba4-pidl = %{samba_depver}
Obsoletes:      samba4-pidl < %{samba_depver}

%description pidl
This package contains the Perl IDL compiler used by Samba
and Wireshark to parse IDL and similar protocols.

%package test
Summary:        Testing tools and libraries for Samba servers and clients
Requires:       %{name} = %{samba_depver} %{name}-common = %{samba_depver} %{name}-winbind = %{samba_depver}
Requires:       %{name}-client-libs = %{samba_depver} %{name}-libs = %{samba_depver}
Requires:       libldb = %{samba_depver}
%if %with_dc
Requires:       %{name}-dc-libs = %{samba_depver}
%endif
%if %with_libsmbclient
Requires:       libsmbclient = %{samba_depver}
%endif
%if %with_libwbclient
Requires:       libwbclient = %{samba_depver}
%endif

Requires: 		python3-%{name} = %{samba_depver}
Requires: 		perl(Archive::Tar)

Provides:       samba4-test = %{samba_depver} %{name}-test-libs = %{samba_depver} %{name}-test-devel = %{samba_depver}
Obsoletes:      samba4-test < %{samba_depver} %{name}-test-libs < %{samba_depver} %{name}-test-devel < %{samba_depver}

%description test
%{name}-test provides testing tools for both the server and client
packages of Samba.

%package usershares
Summary: Provides support for non-root user shares
Requires: %{name} = %{samba_depver}
Requires: %{name}-common-tools = %{samba_depver}

%description usershares
Installing this package will provide a configuration file, group and
directories to support non-root user shares. You can configure them
as a user using the `net usershare` command.

%package winbind
Summary:        The winbind package for %{name}
Requires:       %{name}-common = %{samba_depver} %{name}-common-tools = %{samba_depver}
Requires:       %{name}-client-libs = %{samba_depver} %{name}-winbind-modules = %{samba_depver}
Requires: 		libwbclient = %{samba_depver} %{name}-libs = %{samba_depver}
Requires:       libldb = %{samba_depver}
Provides:       samba4-winbind = %{samba_depver}
Obsoletes:      samba4-winbind < %{samba_depver}

# Old NetworkManager expects the dispatcher scripts in a different place
Conflicts: NetworkManager < 1.20

%description winbind
This package provides the winbind NSS library, and some client
tools.  Winbind enables Linux to be a full member in Windows domains and to use
Windows user and group accounts on Linux.

%package winbind-clients
Summary:        The winbind client package for %{name}
Requires:       %{name}-common = %{samba_depver} %{name}-client-libs = %{samba_depver}
Requires:       %{name}-libs = %{samba_depver} %{name}-winbind = %{samba_depver}
Requires:       libldb = %{samba_depver}
%if %with_libwbclient
Requires:       libwbclient = %{samba_depver}
%endif
Provides:       samba4-winbind-clients = %{samba_depver}
Obsoletes:      samba4-winbind-clients < %{samba_depver}

%description winbind-clients
This package contains the wbinfo and ntlm_auth tool.

%package winbind-krb5-locator
Summary:        Winbind krb5 locator package for %{name}
Requires:       chkconfig
%if %with_libwbclient
Requires:       libwbclient = %{samba_depver} %{name}-winbind = %{samba_depver}
%else
Requires:       %{name}-libs = %{samba_depver}
%endif
Requires: 	samba-client-libs = %{samba_depver}
Requires:       libldb = %{samba_depver}

Provides:       samba4-winbind-krb5-locator = %{samba_depver}
Obsoletes:      samba4-winbind-krb5-locator < %{samba_depver}

%description winbind-krb5-locator
This package is a plugin for the system kerberos library to allow
the local kerberos library to use the same KDC as samba and winbind use

%package winbind-modules
Summary:        The winbind modules for %{name}
Requires:       %{name}-client-libs = %{samba_depver} %{name}-libs = %{samba_depver} pam
%if %with_libwbclient
Requires:       libwbclient = %{samba_depver}
%endif
Obsoletes:      samba-winbind-modules < %{samba_depver}

%description winbind-modules
This package provides the NSS library and a PAM module
necessary to communicate to the Winbind Daemon

### WINEXE
%if %{with_winexe}
%package winexe
Summary: Samba Winexe Windows Binary
License: GPL-3.0-only
Requires: %{name}-client-libs = %{samba_depver}
Requires: %{name}-common-libs = %{samba_depver}
Requires: libldb = %{samba_depver}
Requires: libwbclient = %{samba_depver}

Provides: bundled(libreplace) = %{samba_depver}

%description winexe
Winexe is a Remote Windows®-command executor
%endif

%if %with_clustering_support
%package -n ctdb
Summary:        A Clustered Database package based on Samba's Trivial Database (TDB)
Requires:       %{name}-winbind-clients = %{samba_depver}
Requires:       %{name}-common = %{samba_depver} %{name}-client-libs = %{samba_depver} coreutils psmisc 
Requires:       sed tdb-tools gawk procps-ng net-tools ethtool iproute iptables util-linux systemd-units

%description -n ctdb
This package is a cluster implementation of the TDB database used by Samba and other
projects to store temporary data. If an application is already using TDB for
temporary data it is very easy to convert that application to be cluster aware
and use CTDB instead.

#endif with clustering
%endif

%package_help

### LIBLDB
%package -n libldb
Summary:   A schema-less, ldap like, API and database
License:   LGPL-3.0-or-later
Requires:  libtalloc%{?_isa} >= %{talloc_version}
Requires:  libtdb%{?_isa} >= %{tdb_version}
Requires:  libtevent%{?_isa} >= %{tevent_version}

Provides:  bundled(libreplace) = %{samba_depver} ldb-tools = %{samba_depver}
Obsoletes: libldb < 2.10 ldb-tools < 2.10
Provides:  libldb = 2.10
Provides:  libldb = %{samba_depver} ldb-tools = %{samba_depver}

%description -n libldb
An extensible library that implements an LDAP like API to access remote LDAP
servers, or use local tdb databases.

### LIBLDB-DEVEL
%package -n libldb-devel
Summary:   Developer tools for the LDB library
License:   LGPL-3.0-or-later
Requires:  libldb%{?_isa} = %{samba_depver}
Requires:  libtdb-devel%{?_isa} >= %{tdb_version}
Requires:  libtalloc-devel%{?_isa} >= %{talloc_version}
Requires:  libtevent-devel%{?_isa} >= %{tevent_version}

Obsoletes: libldb-devel < 2.10
Provides:  libldb-devel = 2.10
Provides:  libldb-devel = %{samba_depver}

%description -n libldb-devel
Header files needed to develop programs that link against the LDB library.

### PYTHON3-LDB
%package -n python3-ldb
Summary:   Python bindings for the LDB library
License:   LGPL-3.0-or-later
Requires:  libldb%{?_isa} = %{samba_depver}
Requires:  python3-tdb%{?_isa} >= %{tdb_version}
Requires:  samba-client-libs = %{samba_depver}
%{?python_provide:%python_provide python3-ldb}

Obsoletes: python3-ldb < 2.10
Provides:  python3-ldb = %{samba_depver}
# These were the C bindings, only used by Samba
Obsoletes: python-ldb-devel-common < 2.10
Provides:  python-ldb-devel-common = 2.10
Provides:  python-ldb-devel-common = %{samba_depver}
Obsoletes: python3-ldb-devel < 2.10
Provides:  python3-ldb-devel = 2.10
Provides:  python3-ldb-devel = %{samba_depver}

%description -n python3-ldb
Python bindings for the LDB library

%package -n libldb-help
Summary:        Help package for libldb
Buildarch:      noarch
Requires:       man info

%description -n libldb-help
Man pages and other related documents for libldb.

%prep
zcat %{SOURCE0} | gpgv2 --quiet --keyring %{SOURCE2} %{SOURCE1} -
%autosetup -n %{name}-%{version} -p1

rm -rfv third_party/heimdal

%build
%global _talloc_lib ,!talloc,!pytalloc,!pytalloc-util
%global _tevent_lib ,!tevent,!pytevent
%global _tdb_lib ,!tdb,!pytdb

%global _samba_bundled_libraries !popt%{_talloc_lib}%{_tevent_lib}%{_tdb_lib}

%global _samba_idmap_modules idmap_ad,idmap_rid,idmap_ldap,idmap_hash,idmap_tdb2
%global _samba_pdb_modules pdb_tdbsam,pdb_ldap,pdb_smbpasswd,pdb_wbc_sam,pdb_samba4
%global _samba_auth_modules auth_wbc,auth_unix,auth_server,auth_samba4
%global _samba_vfs_modules vfs_dfs_samba4

%global _samba_modules %{_samba_idmap_modules},%{_samba_pdb_modules},%{_samba_auth_modules},%{_samba_vfs_modules}

%global _libsmbclient %nil
%global _libwbclient %nil

%if ! %with_libsmbclient
%global _libsmbclient smbclient,
%endif

%if ! %with_libwbclient
%global _libwbclient wbclient,
%endif

%global _default_private_libraries !ldb,!dcerpc-samr,!samba-policy,!tevent-util,!dcerpc,!samba-hostconfig,!samba-credentials,!dcerpc_server,!samdb,
%global _samba_private_libraries %{_default_private_libraries}%{_libsmbclient}%{_libwbclient}

export python_LDFLAGS="$(echo %{__global_ldflags} | sed -e 's/-Wl,-z,defs//g')"

%ifnarch riscv64 loongarch64 ppc64le
export LDFLAGS="%{__global_ldflags} -fuse-ld=gold"
%else
export LDFLAGS="%{__global_ldflags} -fuse-ld=bfd"
%endif

%if %{with ccache}
# Add support for mock ccache plugin
CCACHE="$(command -v ccache)"
if [ -n "${CCACHE}" ];then
	${CCACHE} -s
	export CC="${CCACHE} gcc"
fi
%endif

%configure \
        --enable-fhs \
%if %{?openEuler:1}0
        --enable-cups \
        --enable-iprint \
%else
        --disable-cups \
        --disable-iprint \
%endif
        --with-piddir=/run \
        --with-sockets-dir=/run/samba \
        --with-modulesdir=%{_libdir}/samba \
        --with-pammodulesdir=%{_libdir}/security \
        --with-lockdir=/var/lib/samba/lock \
        --with-statedir=/var/lib/samba \
        --with-cachedir=/var/lib/samba \
        --disable-rpath-install \
        --with-shared-modules=%{_samba_modules} \
        --bundled-libraries=%{_samba_bundled_libraries} \
        --private-libraries=%{_samba_private_libraries} \
        --with-pam \
        --with-pie \
        --with-relro \
        --without-fam \
        --with-system-mitkrb5 \
        --with-experimental-mit-ad-dc \
%if ! %with_dc
        --without-ad-dc \
%endif
%if ! %with_vfs_glusterfs
        --disable-glusterfs \
%endif
%if %with_clustering_support
        --with-cluster-support \
%endif
%if %with_profiling
        --with-profiling-data \
%endif
%if %{with testsuite}
        --enable-selftest \
%endif
        --with-systemd \
        --systemd-install-services \
        --with-systemddir=/usr/lib/systemd/system \
        --systemd-smb-extra=%{_systemd_extra} \
        --systemd-nmb-extra=%{_systemd_extra} \
        --systemd-winbind-extra=%{_systemd_extra} \
%if %with_clustering_support
	--systemd-ctdb-extra=%{_systemd_extra} \
%endif
        --systemd-samba-extra=%{_systemd_extra}

%make_build

pushd pidl
%__perl Makefile.PL PREFIX=%{_prefix}

%make_build
popd

pushd lib/ldb
doxygen Doxyfile
popd

%install

%make_install

install -d -m 0755 %{buildroot}/usr/{sbin,bin}
install -d -m 0755 %{buildroot}%{_libdir}/security
install -d -m 0755 %{buildroot}/var/lib/samba
install -d -m 0755 %{buildroot}/var/lib/samba/certs
install -d -m 0755 %{buildroot}/var/lib/samba/drivers
install -d -m 0755 %{buildroot}/var/lib/samba/lock
install -d -m 0755 %{buildroot}/var/lib/samba/private
install -d -m 0755 %{buildroot}/var/lib/samba/private/certs
install -d -m 0755 %{buildroot}/var/lib/samba/scripts
install -d -m 0755 %{buildroot}/var/lib/samba/sysvol
install -d -m 0755 %{buildroot}/var/lib/samba/usershares
install -d -m 0755 %{buildroot}/var/lib/samba/winbindd_privileged
install -d -m 0755 %{buildroot}/var/log/samba/old
install -d -m 0755 %{buildroot}/run/samba
install -d -m 0755 %{buildroot}/run/winbindd
install -d -m 0755 %{buildroot}/%{_libdir}/samba
install -d -m 0755 %{buildroot}/%{_libdir}/samba/ldb
install -d -m 0755 %{buildroot}/%{_libdir}/pkgconfig

%if %{?openEuler:1}0
touch %{buildroot}%{_libexecdir}/samba/cups_backend_smb
%endif

# Install other stuff
install -d -m 0755 %{buildroot}%{_sysconfdir}/logrotate.d
install -m 0644 %{SOURCE3} %{buildroot}%{_sysconfdir}/logrotate.d/samba

install -m 0644 %{SOURCE4} %{buildroot}%{_sysconfdir}/samba/smb.conf
%if %{!?openEuler:1}0
sed  -i -e '/printing = cups/d' -e '/printcap name = cups/d' -e '/load printers = yes/d' -e '/cups options = raw/d' %{buildroot}%{_sysconfdir}/samba/smb.conf
%endif
install -m 0644 %{SOURCE5} %{buildroot}%{_sysconfdir}/samba/smb.conf.example
install -m 0644 %{SOURCE8} %{buildroot}%{_sysconfdir}/samba/usershares.conf

install -d -m 0755 %{buildroot}%{_sysconfdir}/security
install -m 0644 %{SOURCE6} %{buildroot}%{_sysconfdir}/security/pam_winbind.conf

install -d -m 0755 %{buildroot}%{_sysconfdir}/pam.d
install -m 0644 %{SOURCE7} %{buildroot}%{_sysconfdir}/pam.d/samba

echo 127.0.0.1 localhost > %{buildroot}%{_sysconfdir}/samba/lmhosts

# openLDAP database schema
install -d -m 0755 %{buildroot}%{_sysconfdir}/openldap/schema
install -m644 examples/LDAP/samba.schema %{buildroot}%{_sysconfdir}/openldap/schema/samba.schema

install -m 0744 packaging/printing/smbprint %{buildroot}%{_bindir}/smbprint

install -d -m 0755 %{buildroot}%{_tmpfilesdir}
# Create /run/samba.
echo "d /run/samba  755 root root" > %{buildroot}%{_tmpfilesdir}/samba.conf
%if %with_clustering_support
echo "d /run/ctdb 755 root root" > %{buildroot}%{_tmpfilesdir}/ctdb.conf
%endif

install -d -m 0755 %{buildroot}%{_sysusersdir}
install -m 0644 %{SOURCE9} %{buildroot}%{_sysusersdir}/samba.conf
install -m 0644 %{SOURCE10} %{buildroot}%{_sysusersdir}/samba-usershares.conf
install -m 0644 %{SOURCE11} %{buildroot}%{_sysusersdir}/samba-winbind.conf

install -d -m 0755 %{buildroot}%{_sysconfdir}/sysconfig
install -m 0644 packaging/systemd/samba.sysconfig %{buildroot}%{_sysconfdir}/sysconfig/samba
%if %with_clustering_support
cat > %{buildroot}%{_sysconfdir}/sysconfig/ctdb <<EOF
# CTDB configuration is now in %%{_sysconfdir}/ctdb/ctdb.conf
EOF

install -d -m 0755 %{buildroot}%{_sysconfdir}/ctdb
install -m 0644 ctdb/config/ctdb.conf %{buildroot}%{_sysconfdir}/ctdb/ctdb.conf
%endif

install -m 0644 %{SOURCE201} packaging/README.downgrade

# NetworkManager online/offline script
install -d -m 0755 %{buildroot}%{_prefix}/lib/NetworkManager/dispatcher.d/
install -m 0755 packaging/NetworkManager/30-winbind-systemd \
            %{buildroot}%{_prefix}/lib/NetworkManager/dispatcher.d/30-winbind

# winbind krb5 plugins
install -d -m 0755 %{buildroot}%{_libdir}/krb5/plugins/libkrb5
touch %{buildroot}%{_libdir}/krb5/plugins/libkrb5/winbind_krb5_locator.so

# remove rpath and runpath
%chrpath_delete

mkdir -p %{buildroot}/etc/ld.so.conf.d
echo "%{_libdir}/%{name}" > %{buildroot}/etc/ld.so.conf.d/%{name}-%{_arch}.conf

%if ! %with_dc
for i in \
    %{_unitdir}/samba.service \
    %{_sbindir}/samba-gpupdate \
    ; do
    rm -f %{buildroot}$i
done
%endif

%if ! %{with_vfs_glusterfs}
rm -rf %{buildroot}%{_mandir}/man8/vfs_glusterfs.8*
%endif
%if ! %{with_vfs_cephfs}
rm -rf %{buildroot}%{_mandir}/man8/vfs_ceph.8*
rm -rf %{buildroot}%{_mandir}/man8/vfs_ceph_snapshots.8*
%endif

# This makes the right links, as rpmlint requires that
# the ldconfig-created links be recorded in the RPM.
/sbin/ldconfig -N -n %{buildroot}%{_libdir}

%if ! %with_dc
for f in samba/libsamba-python-private-samba.so; do
    rm -f %{buildroot}%{_libdir}/$f
done
#endif ! with_dc
%endif

pushd pidl
make DESTDIR=%{buildroot} install_vendor

rm -f %{buildroot}%{perl_archlib}/perllocal.pod
rm -f %{buildroot}%{perl_archlib}/vendor_perl/auto/Parse/Pidl/.packlist

# Already packaged by perl Parse:Yapp
rm -rf %{buildroot}%{perl_vendorlib}/Parse/Yapp
popd

# Install libldb manpages
cp -a lib/ldb/apidocs/man/* %{buildroot}%{_mandir}
# Remove manpages we don't want
rm -f %{buildroot}%{_mandir}/man3/_*
rm -f %{buildroot}%{_mandir}/man3/PyLdb*

# CTDB
%if %with_clustering_support
touch %{buildroot}%{_libexecdir}/ctdb/statd_callout
#endif with clustering
%endif

%if %{with testsuite}
%check
TDB_NO_FSYNC=1 %make_build test FAIL_IMMEDIATELY=1
#endif with testsuite
%endif

%post
%systemd_post samba-bgqd.service
%systemd_post smb.service
%systemd_post nmb.service
/sbin/ldconfig

%preun
%systemd_preun samba-bgqd.service
%systemd_preun smb.service
%systemd_preun nmb.service

%posttrans
%systemd_postun_with_restart samba-bgqd.service
%systemd_postun_with_restart smb.service
%systemd_postun_with_restart nmb.service
/sbin/ldconfig

%pre common
%sysusers_create_compat %{SOURCE9}

%post common
%{?ldconfig}
%tmpfiles_create %{_tmpfilesdir}/samba.conf
if [ -d /var/cache/samba ]; then
    mv /var/cache/samba/netsamlogon_cache.tdb /var/lib/samba/ 2>/dev/null
    mv /var/cache/samba/winbindd_cache.tdb /var/lib/samba/ 2>/dev/null
    rm -rf /var/cache/samba/
    ln -sf /var/cache/samba /var/lib/samba/
fi

%postun common
/sbin/ldconfig

%post client
/sbin/ldconfig
%if %{?openEuler:1}0
%{_sbindir}/update-alternatives --install %{_libexecdir}/samba/cups_backend_smb \
    cups_backend_smb \
    %{_bindir}/smbspool 10
%endif

%postun client
/sbin/ldconfig
%if %{?openEuler:1}0
if [ $1 -eq 0 ] ; then
    %{_sbindir}/update-alternatives --remove cups_backend_smb %{_bindir}/smbspool
fi
%endif

%ldconfig_scriptlets client-libs

%if %{with_dc}
%ldconfig_scriptlets -n python3-samba-dc
%ldconfig_scriptlets dc-libs
%ldconfig_scriptlets dc-bind-dlz

%post dc
/sbin/ldconfig
%systemd_post samba.service

%preun dc
/sbin/ldconfig
%systemd_preun samba.service

%postun dc
%systemd_postun_with_restart samba.service
/sbin/ldconfig
#endif with_dc
%endif

%post krb5-printing
%if %{?openEuler:1}0
%{_sbindir}/update-alternatives --install %{_libexecdir}/samba/cups_backend_smb \
	cups_backend_smb \
	%{_libexecdir}/samba/smbspool_krb5_wrapper 50
%endif
/sbin/ldconfig

%postun krb5-printing
%if %{?openEuler:1}0
if [ $1 -eq 0 ] ; then
	%{_sbindir}/update-alternatives --remove cups_backend_smb %{_libexecdir}/samba/smbspool_krb5_wrapper
fi
%endif
/sbin/ldconfig

%ldconfig_scriptlets libs

%if %with_libsmbclient
%ldconfig_scriptlets -n libsmbclient
%endif

%if %with_libwbclient
%pre -n libwbclient
if [ $1 -gt 1 ];then
	rm -rf %{_libdir}/samba/wbclient/ 2>/dev/null
	rm -rf /etc/alternatives/libwbclient.so* 2>/dev/null
	rm -rf /var/lib/alternatives/libwbclient.so* 2>/dev/null
fi
%{?ldconfig}

%ldconfig_scriptlets -n libwbclient
#endif with_libwbclient
%endif

%ldconfig_scriptlets test

%pre usershares
%sysusers_create_compat %{SOURCE10}

%pre winbind
%sysusers_create_compat %{SOURCE11}

%post winbind
%systemd_post winbind.service
/sbin/ldconfig

%preun winbind
%systemd_preun winbind.service

%postun winbind
%systemd_postun_with_restart winbind.service
/sbin/ldconfig

%postun winbind-krb5-locator
if [ "$1" -ge "1" ]; then
        if [ "`readlink %{_sysconfdir}/alternatives/winbind_krb5_locator.so`" == "%{_libdir}/samba/krb5/winbind_krb5_locator.so" ]; then
                %{_sbindir}/update-alternatives --set winbind_krb5_locator.so %{_libdir}/samba/krb5/winbind_krb5_locator.so
        fi
fi
/sbin/ldconfig

%post winbind-krb5-locator
%{_sbindir}/update-alternatives --install %{_libdir}/krb5/plugins/libkrb5/winbind_krb5_locator.so \
                                winbind_krb5_locator.so %{_libdir}/samba/krb5/winbind_krb5_locator.so 10
/sbin/ldconfig

%preun winbind-krb5-locator
if [ $1 -eq 0 ]; then
        %{_sbindir}/update-alternatives --remove winbind_krb5_locator.so %{_libdir}/samba/krb5/winbind_krb5_locator.so
fi

%ldconfig_scriptlets winbind-modules
%ldconfig_scriptlets winbind-clients

%if %with_clustering_support

%ldconfig_scriptlets -n ctdb-tests

%post -n ctdb
/usr/bin/systemd-tmpfiles --create %{_tmpfilesdir}/ctdb.conf
%systemd_post ctdb.service
/sbin/ldconfig

%preun -n ctdb
%systemd_preun ctdb.service

%postun -n ctdb
%systemd_postun_with_restart ctdb.service
/sbin/ldconfig
%endif

%ldconfig_scriptlets common-tools

%ldconfig_scriptlets -n python3-samba

%ldconfig_scriptlets -n libldb

%ldconfig_scriptlets -n python3-ldb

### SAMBA
%files
%doc examples/autofs examples/LDAP examples/misc
%doc examples/printer-accounting examples/printing
%doc packaging/README.downgrade

%{_bindir}/smbstatus
%{_sbindir}/eventlogadm
%{_sbindir}/nmbd
%{_sbindir}/smbd
%if %{with_dc}
# This is only used by vfs_dfs_samba4
%{_libdir}/samba/libdfs-server-ad-private-samba.so
%endif
%dir %{_libdir}/samba/auth
%{_libdir}/samba/auth/unix.so
%dir %{_libdir}/samba/vfs
%{_libdir}/samba/vfs/acl_tdb.so
%{_libdir}/samba/vfs/acl_xattr.so
%{_libdir}/samba/vfs/aio_fork.so
%{_libdir}/samba/vfs/aio_pthread.so
%{_libdir}/samba/vfs/audit.so
%{_libdir}/samba/vfs/btrfs.so
%{_libdir}/samba/vfs/cap.so
%{_libdir}/samba/vfs/catia.so
%{_libdir}/samba/vfs/commit.so
%{_libdir}/samba/vfs/crossrename.so
%{_libdir}/samba/vfs/default_quota.so
%if %{with_dc}
%{_libdir}/samba/vfs/dfs_samba4.so
%endif
%{_libdir}/samba/vfs/dirsort.so
%{_libdir}/samba/vfs/expand_msdfs.so
%{_libdir}/samba/vfs/extd_audit.so
%{_libdir}/samba/vfs/fake_perms.so
%{_libdir}/samba/vfs/fileid.so
%{_libdir}/samba/vfs/fruit.so
%{_libdir}/samba/vfs/full_audit.so
%{_libdir}/samba/vfs/gpfs.so
%{_libdir}/samba/vfs/glusterfs_fuse.so
%if %{with_vfs_io_uring}
%{_libdir}/samba/vfs/io_uring.so
%endif
%{_libdir}/samba/vfs/linux_xfs_sgid.so
%{_libdir}/samba/vfs/media_harmony.so
%{_libdir}/samba/vfs/offline.so
%{_libdir}/samba/vfs/preopen.so
%{_libdir}/samba/vfs/readahead.so
%{_libdir}/samba/vfs/readonly.so
%{_libdir}/samba/vfs/recycle.so
%{_libdir}/samba/vfs/shadow_copy.so
%{_libdir}/samba/vfs/shadow_copy2.so
%{_libdir}/samba/vfs/shell_snap.so
%{_libdir}/samba/vfs/snapper.so
%{_libdir}/samba/vfs/streams_depot.so
%{_libdir}/samba/vfs/streams_xattr.so
%{_libdir}/samba/vfs/syncops.so
%{_libdir}/samba/vfs/time_audit.so
%{_libdir}/samba/vfs/unityed_media.so
%{_libdir}/samba/vfs/virusfilter.so
%{_libdir}/samba/vfs/widelinks.so
%{_libdir}/samba/vfs/worm.so
%{_libdir}/samba/vfs/xattr_tdb.so
%dir %{_libexecdir}/samba
%{_libexecdir}/samba/samba-bgqd
%{_libexecdir}/samba/samba-dcerpcd
%{_libexecdir}/samba/rpcd_classic
%{_libexecdir}/samba/rpcd_epmapper
%{_libexecdir}/samba/rpcd_fsrvp
%{_libexecdir}/samba/rpcd_lsad
%{_libexecdir}/samba/rpcd_mdssvc
%{_libexecdir}/samba/rpcd_spoolss
%{_libexecdir}/samba/rpcd_winreg
%{_libexecdir}/samba/rpcd_witness
%dir %{_datadir}/samba
%dir %{_datadir}/samba/mdssvc
%{_datadir}/samba/mdssvc/elasticsearch_mappings.json

%{_unitdir}/nmb.service
%{_unitdir}/smb.service
%{_unitdir}/samba-bgqd.service
%dir %{_sysconfdir}/openldap/schema
%config %{_sysconfdir}/openldap/schema/samba.schema
%config(noreplace) %{_sysconfdir}/pam.d/samba

%attr(775,root,printadmin) %dir /var/lib/samba/drivers
%config(noreplace) /etc/ld.so.conf.d/*

%files libs
%{_libdir}/libdcerpc-samr.so.*

%{_libdir}/samba/libLIBWBCLIENT-OLD-private-samba.so
%{_libdir}/samba/libauth-unix-token-private-samba.so
%{_libdir}/samba/libdcerpc-samba4-private-samba.so
%{_libdir}/samba/libdnsserver-common-private-samba.so
%{_libdir}/samba/libshares-private-samba.so
%{_libdir}/samba/libsmbpasswdparser-private-samba.so
%{_libdir}/samba/libxattr-tdb-private-samba.so
%{_libdir}/samba/libREG-FULL-private-samba.so
%{_libdir}/samba/libRPC-SERVER-LOOP-private-samba.so
%{_libdir}/samba/libRPC-WORKER-private-samba.so
%config(noreplace) /etc/ld.so.conf.d/*

%files client
%doc source3/client/README.smbspool
%{_bindir}/cifsdd
%{_bindir}/dbwrap_tool
%{_bindir}/dumpmscat
%{_bindir}/mvxattr
%{_bindir}/mdsearch
%{_bindir}/nmblookup
%{_bindir}/oLschema2ldif
%{_bindir}/regdiff
%{_bindir}/regpatch
%{_bindir}/regshell
%{_bindir}/regtree
%{_bindir}/rpcclient
%{_bindir}/samba-regedit
%{_bindir}/sharesec
%{_bindir}/smbcacls
%{_bindir}/smbclient
%{_bindir}/smbcquotas
%{_bindir}/smbget
%{_bindir}/smbprint
%{_bindir}/smbspool
%{_bindir}/smbtar
%{_bindir}/smbtree
%{_bindir}/wspsearch
%if %{?openEuler:1}0
%dir %{_libexecdir}/samba
%ghost %{_libexecdir}/samba/cups_backend_smb
%endif
%config(noreplace) /etc/ld.so.conf.d/*

%files client-libs
%{_libdir}/libdcerpc-binding.so.*
%{_libdir}/libdcerpc-server-core.so.*
%{_libdir}/libdcerpc.so.*
%{_libdir}/libndr-krb5pac.so.*
%{_libdir}/libndr-nbt.so.*
%{_libdir}/libndr-standard.so.*
%{_libdir}/libndr.so.*
%{_libdir}/libnetapi.so.*
%{_libdir}/libsamba-credentials.so.*
%{_libdir}/libsamba-errors.so.*
%{_libdir}/libsamba-hostconfig.so.*
%{_libdir}/libsamba-passdb.so.*
%{_libdir}/libsamba-util.so.*
%{_libdir}/libsamdb.so.*
%{_libdir}/libsmbconf.so.*
%{_libdir}/libsmbldap.so.*
%{_libdir}/libtevent-util.so.*

%dir %{_libdir}/samba
%{_libdir}/samba/libCHARSET3-private-samba.so
%{_libdir}/samba/libMESSAGING-SEND-private-samba.so
%{_libdir}/samba/libMESSAGING-private-samba.so
%{_libdir}/samba/libaddns-private-samba.so
%{_libdir}/samba/libads-private-samba.so
%{_libdir}/samba/libasn1util-private-samba.so
%{_libdir}/samba/libauth-private-samba.so
%{_libdir}/samba/libauthkrb5-private-samba.so
%{_libdir}/samba/libcli-cldap-private-samba.so
%{_libdir}/samba/libcli-ldap-common-private-samba.so
%{_libdir}/samba/libcli-ldap-private-samba.so
%{_libdir}/samba/libcli-nbt-private-samba.so
%{_libdir}/samba/libcli-smb-common-private-samba.so
%{_libdir}/samba/libcli-spoolss-private-samba.so
%{_libdir}/samba/libcliauth-private-samba.so
%{_libdir}/samba/libclidns-private-samba.so
%{_libdir}/samba/libcluster-private-samba.so
%{_libdir}/samba/libcmdline-contexts-private-samba.so
%{_libdir}/samba/libcommon-auth-private-samba.so
%{_libdir}/samba/libctdb-event-client-private-samba.so
%{_libdir}/samba/libdbwrap-private-samba.so
%{_libdir}/samba/libdcerpc-pkt-auth-private-samba.so
%{_libdir}/samba/libdcerpc-samba-private-samba.so
%{_libdir}/samba/libevents-private-samba.so
%{_libdir}/samba/libflag-mapping-private-samba.so
%{_libdir}/samba/libgenrand-private-samba.so
%{_libdir}/samba/libgensec-private-samba.so
%{_libdir}/samba/libgpext-private-samba.so
%{_libdir}/samba/libgpo-private-samba.so
%{_libdir}/samba/libgse-private-samba.so
%{_libdir}/samba/libhttp-private-samba.so
%{_libdir}/samba/libinterfaces-private-samba.so
%{_libdir}/samba/libiov-buf-private-samba.so
%{_libdir}/samba/libkrb5samba-private-samba.so
%{_libdir}/samba/libldbsamba-private-samba.so
%{_libdir}/samba/liblibcli-lsa3-private-samba.so
%{_libdir}/samba/liblibcli-netlogon3-private-samba.so
%{_libdir}/samba/liblibsmb-private-samba.so
%{_libdir}/samba/libmessages-dgm-private-samba.so
%{_libdir}/samba/libmessages-util-private-samba.so
%{_libdir}/samba/libmscat-private-samba.so
%{_libdir}/samba/libmsghdr-private-samba.so
%{_libdir}/samba/libmsrpc3-private-samba.so
%{_libdir}/samba/libndr-samba-private-samba.so
%{_libdir}/samba/libndr-samba4-private-samba.so
%{_libdir}/samba/libnet-keytab-private-samba.so
%{_libdir}/samba/libnetif-private-samba.so
%{_libdir}/samba/libnpa-tstream-private-samba.so
%{_libdir}/samba/libposix-eadb-private-samba.so
%{_libdir}/samba/libprinter-driver-private-samba.so
%{_libdir}/samba/libprinting-migrate-private-samba.so
%{_libdir}/samba/libreplace-private-samba.so
%{_libdir}/samba/libregistry-private-samba.so
%{_libdir}/samba/libsamba-cluster-support-private-samba.so
%{_libdir}/samba/libsamba-debug-private-samba.so
%{_libdir}/samba/libsamba-modules-private-samba.so
%{_libdir}/samba/libsamba-security-private-samba.so
%{_libdir}/samba/libsamba-sockets-private-samba.so
%{_libdir}/samba/libsamba3-util-private-samba.so
%{_libdir}/samba/libsamdb-common-private-samba.so
%{_libdir}/samba/libsecrets3-private-samba.so
%{_libdir}/samba/libserver-id-db-private-samba.so
%{_libdir}/samba/libserver-role-private-samba.so
%{_libdir}/samba/libsmb-transport-private-samba.so
%{_libdir}/samba/libsmbclient-raw-private-samba.so
%{_libdir}/samba/libsmbd-base-private-samba.so
%{_libdir}/samba/libsmbd-shim-private-samba.so
%{_libdir}/samba/libsmbldaphelper-private-samba.so
%{_libdir}/samba/libstable-sort-private-samba.so
%{_libdir}/samba/libsys-rw-private-samba.so
%{_libdir}/samba/libsocket-blocking-private-samba.so
%{_libdir}/samba/libtalloc-report-printf-private-samba.so
%{_libdir}/samba/libtalloc-report-private-samba.so
%{_libdir}/samba/libtdb-wrap-private-samba.so
%{_libdir}/samba/libtime-basic-private-samba.so
%{_libdir}/samba/libtorture-private-samba.so
%{_libdir}/samba/libutil-crypt-private-samba.so
%{_libdir}/samba/libutil-reg-private-samba.so
%{_libdir}/samba/libutil-setid-private-samba.so
%{_libdir}/samba/libutil-tdb-private-samba.so
%config(noreplace) /etc/ld.so.conf.d/*

%if ! %with_libwbclient
%{_libdir}/samba/libwbclient.so.*
#endif ! with_libwbclient
%endif

%if ! %with_libsmbclient
%{_libdir}/samba/libsmbclient.so.*
#endif ! with_libsmbclient
%endif

%files common
%license COPYING
%doc README.md WHATSNEW.txt
%{_tmpfilesdir}/samba.conf
%{_sysusersdir}/samba.conf
%dir %{_sysconfdir}/logrotate.d/
%config(noreplace) %{_sysconfdir}/logrotate.d/samba
%attr(0700,root,root) %dir /var/log/samba
%attr(0700,root,root) %dir /var/log/samba/old
%ghost %dir /run/samba
%ghost %dir /run/winbindd
%dir /var/lib/samba
%dir /var/lib/samba/certs
%attr(700,root,root) %dir /var/lib/samba/private
%attr(700,root,root) %dir /var/lib/samba/private/certs
%dir /var/lib/samba/lock
%attr(755,root,root) %dir %{_sysconfdir}/samba
%config(noreplace) %{_sysconfdir}/samba/smb.conf
%{_sysconfdir}/samba/smb.conf.example
%config(noreplace) %{_sysconfdir}/samba/lmhosts
%config(noreplace) %{_sysconfdir}/sysconfig/samba

%{_libdir}/samba/libcmdline-private-samba.so

%dir %{_libdir}/samba/ldb

%dir %{_libdir}/samba/pdb
%{_libdir}/samba/pdb/ldapsam.so
%{_libdir}/samba/pdb/smbpasswd.so
%{_libdir}/samba/pdb/tdbsam.so
%config(noreplace) /etc/ld.so.conf.d/*

%files common-tools
%{_bindir}/net
%{_bindir}/pdbedit
%{_bindir}/profiles
%{_bindir}/samba-log-parser
%{_bindir}/smbcontrol
%{_bindir}/smbpasswd
%{_bindir}/testparm
%config(noreplace) /etc/ld.so.conf.d/*

%files tools
%{_bindir}/samba-tool

%if %{with_dc}
%files dc
%{_unitdir}/samba.service
%{_sbindir}/samba
%{_sbindir}/samba_dnsupdate
%{_sbindir}/samba_downgrade_db
%{_sbindir}/samba-gpupdate
%{_sbindir}/samba_kcc
%{_sbindir}/samba_spnupdate
%{_sbindir}/samba_upgradedns

%{_libdir}/krb5/plugins/kdb/samba.so

%{_libdir}/samba/auth/samba4.so
%dir %{_libdir}/samba/gensec
%{_libdir}/samba/gensec/krb5.so
%{_libdir}/samba/ldb/acl.so
%{_libdir}/samba/ldb/aclread.so
%{_libdir}/samba/ldb/anr.so
%{_libdir}/samba/ldb/audit_log.so
%{_libdir}/samba/ldb/count_attrs.so
%{_libdir}/samba/ldb/descriptor.so
%{_libdir}/samba/ldb/dirsync.so
%{_libdir}/samba/ldb/dns_notify.so
%{_libdir}/samba/ldb/dsdb_notification.so
%{_libdir}/samba/ldb/encrypted_secrets.so
%{_libdir}/samba/ldb/extended_dn_in.so
%{_libdir}/samba/ldb/extended_dn_out.so
%{_libdir}/samba/ldb/extended_dn_store.so
%{_libdir}/samba/ldb/group_audit_log.so
%{_libdir}/samba/ldb/ildap.so
%{_libdir}/samba/ldb/ldap.so
%{_libdir}/samba/ldb/instancetype.so
%{_libdir}/samba/ldb/lazy_commit.so
%{_libdir}/samba/ldb/ldbsamba_extensions.so
%{_libdir}/samba/ldb/linked_attributes.so
%{_libdir}/samba/ldb/new_partition.so
%{_libdir}/samba/ldb/objectclass.so
%{_libdir}/samba/ldb/objectclass_attrs.so
%{_libdir}/samba/ldb/objectguid.so
%{_libdir}/samba/ldb/operational.so
%{_libdir}/samba/ldb/paged_results.so
%{_libdir}/samba/ldb/partition.so
%{_libdir}/samba/ldb/password_hash.so
%{_libdir}/samba/ldb/ranged_results.so
%{_libdir}/samba/ldb/repl_meta_data.so
%{_libdir}/samba/ldb/resolve_oids.so
%{_libdir}/samba/ldb/rootdse.so
%{_libdir}/samba/ldb/samba3sam.so
%{_libdir}/samba/ldb/samba3sid.so
%{_libdir}/samba/ldb/samba_dsdb.so
%{_libdir}/samba/ldb/samba_secrets.so
%{_libdir}/samba/ldb/samldb.so
%{_libdir}/samba/ldb/schema_data.so
%{_libdir}/samba/ldb/schema_load.so
%{_libdir}/samba/ldb/secrets_tdb_sync.so
%{_libdir}/samba/ldb/show_deleted.so
%{_libdir}/samba/ldb/subtree_delete.so
%{_libdir}/samba/ldb/subtree_rename.so
%{_libdir}/samba/ldb/tombstone_reanimate.so
%{_libdir}/samba/ldb/unique_object_sids.so
%{_libdir}/samba/ldb/update_keytab.so
%{_libdir}/samba/ldb/vlv.so
%{_libdir}/samba/ldb/wins_ldb.so
%{_libdir}/samba/vfs/posix_eadb.so
%config(noreplace) /etc/ld.so.conf.d/*
%dir /var/lib/samba/sysvol

%files dc-provision
%license source4/setup/ad-schema/licence.txt
%{_datadir}/samba/setup
#endif with_dc
%endif

%files dc-libs
%{_libdir}/libsamba-policy.so.0*
%{_libdir}/samba/libauth4-private-samba.so
%{_libdir}/samba/libsamba-net-private-samba.so
%if %{with dc}
%{_libdir}/samba/libdb-glue-private-samba.so
%{_libdir}/samba/libpac-private-samba.so
%{_libdir}/samba/libprocess-model-private-samba.so
%{_libdir}/samba/libservice-private-samba.so
%dir %{_libdir}/samba/process_model
%{_libdir}/samba/process_model/prefork.so
%{_libdir}/samba/process_model/standard.so
%dir %{_libdir}/samba/service
%{_libdir}/samba/service/cldap.so
%{_libdir}/samba/service/dcerpc.so
%{_libdir}/samba/service/dns.so
%{_libdir}/samba/service/dns_update.so
%{_libdir}/samba/service/drepl.so
%{_libdir}/samba/service/kcc.so
%{_libdir}/samba/service/kdc.so
%{_libdir}/samba/service/ldap.so
%{_libdir}/samba/service/nbtd.so
%{_libdir}/samba/service/ntp_signd.so
%{_libdir}/samba/service/s3fs.so
%{_libdir}/samba/service/winbindd.so
%{_libdir}/samba/service/wrepl.so
%{_libdir}/libdcerpc-server.so.*
%{_libdir}/samba/libad-claims-private-samba.so
%{_libdir}/samba/libauthn-policy-util-private-samba.so
%{_libdir}/samba/libdsdb-module-private-samba.so
%{_libdir}/samba/libdsdb-garbage-collect-tombstones-private-samba.so
%{_libdir}/samba/libscavenge-dns-records-private-samba.so
%config(noreplace) /etc/ld.so.conf.d/*

%files dc-bind-dlz
%attr(770,root,named) %dir /var/lib/samba/bind-dns
%dir %{_libdir}/samba/bind9
%{_libdir}/samba/bind9/dlz_bind9_10.so
%{_libdir}/samba/bind9/dlz_bind9_11.so
%{_libdir}/samba/bind9/dlz_bind9_12.so
%{_libdir}/samba/bind9/dlz_bind9_14.so            
%{_libdir}/samba/bind9/dlz_bind9_16.so
%{_libdir}/samba/bind9/dlz_bind9_18.so
%config(noreplace) /etc/ld.so.conf.d/*
#endif with_dc
%endif

%files devel
%{_includedir}/samba-4.0/charset.h
%{_includedir}/samba-4.0/core/doserr.h
%{_includedir}/samba-4.0/core/error.h
%{_includedir}/samba-4.0/core/hresult.h
%{_includedir}/samba-4.0/core/ntstatus.h
%{_includedir}/samba-4.0/core/ntstatus_gen.h
%{_includedir}/samba-4.0/core/werror.h
%{_includedir}/samba-4.0/core/werror_gen.h
%{_includedir}/samba-4.0/credentials.h
%{_includedir}/samba-4.0/dcerpc.h
%{_includedir}/samba-4.0/dcesrv_core.h
%{_includedir}/samba-4.0/domain_credentials.h
%{_includedir}/samba-4.0/gen_ndr/atsvc.h
%{_includedir}/samba-4.0/gen_ndr/auth.h
%{_includedir}/samba-4.0/gen_ndr/claims.h
%{_includedir}/samba-4.0/gen_ndr/dcerpc.h
%{_includedir}/samba-4.0/gen_ndr/krb5pac.h
%{_includedir}/samba-4.0/gen_ndr/lsa.h
%{_includedir}/samba-4.0/gen_ndr/misc.h
%{_includedir}/samba-4.0/gen_ndr/nbt.h
%{_includedir}/samba-4.0/gen_ndr/drsblobs.h
%{_includedir}/samba-4.0/gen_ndr/drsuapi.h
%{_includedir}/samba-4.0/gen_ndr/ndr_drsblobs.h
%{_includedir}/samba-4.0/gen_ndr/ndr_drsuapi.h
%{_includedir}/samba-4.0/gen_ndr/ndr_atsvc.h
%{_includedir}/samba-4.0/gen_ndr/ndr_dcerpc.h
%{_includedir}/samba-4.0/gen_ndr/ndr_krb5pac.h
%{_includedir}/samba-4.0/gen_ndr/ndr_misc.h
%{_includedir}/samba-4.0/gen_ndr/ndr_nbt.h
%{_includedir}/samba-4.0/gen_ndr/ndr_samr.h
%{_includedir}/samba-4.0/gen_ndr/ndr_samr_c.h
%{_includedir}/samba-4.0/gen_ndr/ndr_svcctl.h
%{_includedir}/samba-4.0/gen_ndr/ndr_svcctl_c.h
%{_includedir}/samba-4.0/gen_ndr/netlogon.h
%{_includedir}/samba-4.0/gen_ndr/samr.h
%{_includedir}/samba-4.0/gen_ndr/security.h
%{_includedir}/samba-4.0/gen_ndr/server_id.h
%{_includedir}/samba-4.0/gen_ndr/svcctl.h
%{_includedir}/samba-4.0/ldb_wrap.h
%{_includedir}/samba-4.0/lookup_sid.h
%{_includedir}/samba-4.0/machine_sid.h
%{_includedir}/samba-4.0/ndr.h
%dir %{_includedir}/samba-4.0/ndr
%{_includedir}/samba-4.0/ndr/ndr_dcerpc.h
%{_includedir}/samba-4.0/ndr/ndr_drsblobs.h
%{_includedir}/samba-4.0/ndr/ndr_drsuapi.h
%{_includedir}/samba-4.0/ndr/ndr_krb5pac.h
%{_includedir}/samba-4.0/ndr/ndr_svcctl.h
%{_includedir}/samba-4.0/ndr/ndr_nbt.h
%{_includedir}/samba-4.0/netapi.h
%{_includedir}/samba-4.0/param.h
%{_includedir}/samba-4.0/passdb.h
%{_includedir}/samba-4.0/policy.h
%{_includedir}/samba-4.0/rpc_common.h
%{_includedir}/samba-4.0/samba/session.h
%{_includedir}/samba-4.0/samba/version.h
%{_includedir}/samba-4.0/share.h
%{_includedir}/samba-4.0/smb2_lease_struct.h
%{_includedir}/samba-4.0/smb3posix.h
%{_includedir}/samba-4.0/smbconf.h
%{_includedir}/samba-4.0/smb_ldap.h
%{_includedir}/samba-4.0/smbldap.h
%{_includedir}/samba-4.0/tdr.h
%{_includedir}/samba-4.0/tsocket.h
%{_includedir}/samba-4.0/tsocket_internal.h
%dir %{_includedir}/samba-4.0/util
%{_includedir}/samba-4.0/util/attr.h
%{_includedir}/samba-4.0/util/blocking.h
%{_includedir}/samba-4.0/util/data_blob.h
%{_includedir}/samba-4.0/util/debug.h
%{_includedir}/samba-4.0/util/discard.h
%{_includedir}/samba-4.0/util/fault.h
%{_includedir}/samba-4.0/util/genrand.h
%{_includedir}/samba-4.0/util/idtree.h
%{_includedir}/samba-4.0/util/idtree_random.h
%{_includedir}/samba-4.0/util/signal.h
%{_includedir}/samba-4.0/util/substitute.h
%{_includedir}/samba-4.0/util/tevent_ntstatus.h
%{_includedir}/samba-4.0/util/tevent_unix.h
%{_includedir}/samba-4.0/util/tevent_werror.h
%{_includedir}/samba-4.0/util/time.h
%{_includedir}/samba-4.0/util/tfork.h
%{_includedir}/samba-4.0/util_ldb.h
%{_libdir}/libdcerpc-binding.so
%{_libdir}/libdcerpc-samr.so
%{_libdir}/libdcerpc.so
%{_libdir}/libndr-krb5pac.so
%{_libdir}/libndr-nbt.so
%{_libdir}/libndr-standard.so
%{_libdir}/libndr.so
%{_libdir}/libnetapi.so
%{_libdir}/libsamba-credentials.so
%{_libdir}/libsamba-errors.so
%{_libdir}/libsamba-hostconfig.so
%{_libdir}/libsamba-util.so
%{_libdir}/libsamdb.so
%{_libdir}/libsmbconf.so
%{_libdir}/libtevent-util.so
%{_libdir}/pkgconfig/dcerpc.pc
%{_libdir}/pkgconfig/dcerpc_samr.pc
%{_libdir}/pkgconfig/ndr.pc
%{_libdir}/pkgconfig/ndr_krb5pac.pc
%{_libdir}/pkgconfig/ndr_nbt.pc
%{_libdir}/pkgconfig/ndr_standard.pc
%{_libdir}/pkgconfig/netapi.pc
%{_libdir}/pkgconfig/samba-credentials.pc
%{_libdir}/pkgconfig/samba-hostconfig.pc
%{_libdir}/pkgconfig/samba-policy.pc
%{_libdir}/pkgconfig/samba-util.pc
%{_libdir}/pkgconfig/samdb.pc
%{_libdir}/libsamba-passdb.so
%{_libdir}/libsamba-policy.so
%{_libdir}/libsmbldap.so

%if %with_dc
%{_includedir}/samba-4.0/dcerpc_server.h
%{_libdir}/libdcerpc-server.so
%{_libdir}/libdcerpc-server-core.so
%{_libdir}/pkgconfig/dcerpc_server.pc
%endif

%if ! %with_libsmbclient
%{_includedir}/samba-4.0/libsmbclient.h
#endif ! with_libsmbclient
%endif

%if ! %with_libwbclient
%{_includedir}/samba-4.0/wbclient.h
#endif ! with_libwbclient
%endif

%if %{with_vfs_cephfs}
%files vfs-cephfs
%{_libdir}/samba/vfs/ceph.so
%{_libdir}/samba/vfs/ceph_new.so
%{_libdir}/samba/vfs/ceph_snapshots.so
%endif

%if %{with_vfs_glusterfs}
%files vfs-glusterfs
%{_libdir}/samba/vfs/glusterfs.so
%endif

%files krb5-printing
%if %{?openEuler:1}0
%attr(0700,root,root) %{_libexecdir}/samba/smbspool_krb5_wrapper
%endif
%config(noreplace) /etc/ld.so.conf.d/*

%if %with_libsmbclient
%files -n libsmbclient
%{_libdir}/libsmbclient.so.*
%config(noreplace) /etc/ld.so.conf.d/*

%files -n libsmbclient-devel
%{_includedir}/samba-4.0/libsmbclient.h
%{_libdir}/libsmbclient.so
%{_libdir}/pkgconfig/smbclient.pc
#endif with_libsmbclient
%endif

%if %with_libwbclient
%files -n libwbclient
%{_libdir}/libwbclient.so.*
%config(noreplace) /etc/ld.so.conf.d/*

%files -n libwbclient-devel
%{_includedir}/samba-4.0/wbclient.h
%{_libdir}/libwbclient.so
%{_libdir}/pkgconfig/wbclient.pc
#endif with_libwbclient
%endif

%files pidl
%doc pidl/README
%attr(755,root,root) %{_bindir}/pidl
%dir %{perl_vendorlib}/Parse
%attr(644,root,root) %{perl_vendorlib}/Parse/Pidl.pm
%dir %{perl_vendorlib}/Parse/Pidl
%attr(644,root,root) %{perl_vendorlib}/Parse/Pidl/Base.pm
%attr(644,root,root) %{perl_vendorlib}/Parse/Pidl/CUtil.pm
%attr(644,root,root) %{perl_vendorlib}/Parse/Pidl/Samba4.pm
%attr(644,root,root) %{perl_vendorlib}/Parse/Pidl/Expr.pm
%attr(644,root,root) %{perl_vendorlib}/Parse/Pidl/ODL.pm
%attr(644,root,root) %{perl_vendorlib}/Parse/Pidl/Typelist.pm
%attr(644,root,root) %{perl_vendorlib}/Parse/Pidl/IDL.pm
%attr(644,root,root) %{perl_vendorlib}/Parse/Pidl/Compat.pm
%dir %{perl_vendorlib}/Parse/Pidl/Wireshark
%attr(644,root,root) %{perl_vendorlib}/Parse/Pidl/Wireshark/Conformance.pm
%attr(644,root,root) %{perl_vendorlib}/Parse/Pidl/Wireshark/NDR.pm
%attr(644,root,root) %{perl_vendorlib}/Parse/Pidl/Dump.pm
%dir %{perl_vendorlib}/Parse/Pidl/Samba3
%attr(644,root,root) %{perl_vendorlib}/Parse/Pidl/Samba3/ServerNDR.pm
%attr(644,root,root) %{perl_vendorlib}/Parse/Pidl/Samba3/ClientNDR.pm
%attr(644,root,root) %{perl_vendorlib}/Parse/Pidl/Samba3/Template.pm
%dir %{perl_vendorlib}/Parse/Pidl/Samba4
%attr(644,root,root) %{perl_vendorlib}/Parse/Pidl/Samba4/Header.pm
%dir %{perl_vendorlib}/Parse/Pidl/Samba4/COM
%attr(644,root,root) %{perl_vendorlib}/Parse/Pidl/Samba4/COM/Header.pm
%attr(644,root,root) %{perl_vendorlib}/Parse/Pidl/Samba4/COM/Proxy.pm
%attr(644,root,root) %{perl_vendorlib}/Parse/Pidl/Samba4/COM/Stub.pm
%attr(644,root,root) %{perl_vendorlib}/Parse/Pidl/Samba4/Python.pm
%attr(644,root,root) %{perl_vendorlib}/Parse/Pidl/Samba4/Template.pm
%dir %{perl_vendorlib}/Parse/Pidl/Samba4/NDR
%attr(644,root,root) %{perl_vendorlib}/Parse/Pidl/Samba4/NDR/Server.pm
%attr(644,root,root) %{perl_vendorlib}/Parse/Pidl/Samba4/NDR/ServerCompat.pm
%attr(644,root,root) %{perl_vendorlib}/Parse/Pidl/Samba4/NDR/Client.pm
%attr(644,root,root) %{perl_vendorlib}/Parse/Pidl/Samba4/NDR/Parser.pm
%attr(644,root,root) %{perl_vendorlib}/Parse/Pidl/Samba4/TDR.pm
%attr(644,root,root) %{perl_vendorlib}/Parse/Pidl/NDR.pm
%attr(644,root,root) %{perl_vendorlib}/Parse/Pidl/Util.pm

%files -n python3-%{name}
%dir %{python3_sitearch}/samba/
%{python3_sitearch}/samba/__init__.py
%dir %{python3_sitearch}/samba/__pycache__
%{python3_sitearch}/samba/__pycache__/__init__.*.pyc
%{python3_sitearch}/samba/__pycache__/auth_util.*.pyc
%{python3_sitearch}/samba/__pycache__/colour.*.pyc
%{python3_sitearch}/samba/__pycache__/common.*.pyc
%{python3_sitearch}/samba/__pycache__/dbchecker.*.pyc
%{python3_sitearch}/samba/__pycache__/descriptor.*.pyc
%{python3_sitearch}/samba/__pycache__/dnsresolver.*.pyc
%{python3_sitearch}/samba/__pycache__/drs_utils.*.pyc
%{python3_sitearch}/samba/__pycache__/functional_level.*.pyc
%{python3_sitearch}/samba/__pycache__/getopt.*.pyc
%{python3_sitearch}/samba/__pycache__/gkdi.*.pyc
%{python3_sitearch}/samba/__pycache__/graph.*.pyc
%{python3_sitearch}/samba/__pycache__/hostconfig.*.pyc
%{python3_sitearch}/samba/__pycache__/idmap.*.pyc
%{python3_sitearch}/samba/__pycache__/join.*.pyc
%{python3_sitearch}/samba/__pycache__/lsa_utils.*.pyc
%{python3_sitearch}/samba/__pycache__/logger.*.pyc
%{python3_sitearch}/samba/__pycache__/mdb_util.*.pyc
%{python3_sitearch}/samba/__pycache__/ms_display_specifiers.*.pyc
%{python3_sitearch}/samba/__pycache__/ms_schema.*.pyc
%{python3_sitearch}/samba/__pycache__/ndr.*.pyc
%{python3_sitearch}/samba/__pycache__/ntacls.*.pyc
%{python3_sitearch}/samba/__pycache__/nt_time.*.pyc
%{python3_sitearch}/samba/__pycache__/policies.*.pyc
%{python3_sitearch}/samba/__pycache__/safe_tarfile.*.pyc
%{python3_sitearch}/samba/__pycache__/sd_utils.*.pyc
%{python3_sitearch}/samba/__pycache__/sites.*.pyc
%{python3_sitearch}/samba/__pycache__/subnets.*.pyc
%{python3_sitearch}/samba/__pycache__/tdb_util.*.pyc
%{python3_sitearch}/samba/__pycache__/upgrade.*.pyc
%{python3_sitearch}/samba/__pycache__/upgradehelpers.*.pyc
%{python3_sitearch}/samba/__pycache__/xattr.*.pyc
%{python3_sitearch}/samba/_glue.*.so
%{python3_sitearch}/samba/_ldb.*.so
%{python3_sitearch}/samba/auth.*.so
%{python3_sitearch}/samba/auth_util.py
%{python3_sitearch}/samba/dbchecker.py
%{python3_sitearch}/samba/colour.py
%{python3_sitearch}/samba/common.py
%{python3_sitearch}/samba/compression.*.so
%{python3_sitearch}/samba/credentials.*.so
%{python3_sitearch}/samba/crypto.*.so
%dir %{python3_sitearch}/samba/dcerpc
%dir %{python3_sitearch}/samba/dcerpc/__pycache__
%{python3_sitearch}/samba/dcerpc/__pycache__/__init__.*.pyc
%{python3_sitearch}/samba/dcerpc/__init__.py
%{python3_sitearch}/samba/dcerpc/atsvc.*.so
%{python3_sitearch}/samba/dcerpc/auth.*.so
%{python3_sitearch}/samba/dcerpc/base.*.so
%{python3_sitearch}/samba/dcerpc/claims.*.so
%{python3_sitearch}/samba/dcerpc/conditional_ace.*.so
%{python3_sitearch}/samba/dcerpc/dcerpc.*.so
%{python3_sitearch}/samba/dcerpc/dfs.*.so
%{python3_sitearch}/samba/dcerpc/dns.*.so
%{python3_sitearch}/samba/dcerpc/dnsp.*.so
%{python3_sitearch}/samba/dcerpc/drsblobs.*.so
%{python3_sitearch}/samba/dcerpc/drsuapi.*.so
%{python3_sitearch}/samba/dcerpc/echo.*.so
%{python3_sitearch}/samba/dcerpc/epmapper.*.so
%{python3_sitearch}/samba/dcerpc/gkdi.*.so
%{python3_sitearch}/samba/dcerpc/gmsa.*.so
%{python3_sitearch}/samba/dcerpc/idmap.*.so
%{python3_sitearch}/samba/dcerpc/initshutdown.*.so
%{python3_sitearch}/samba/dcerpc/irpc.*.so
%{python3_sitearch}/samba/dcerpc/krb5ccache.*.so
%{python3_sitearch}/samba/dcerpc/krb5pac.*.so
%{python3_sitearch}/samba/dcerpc/lsa.*.so
%{python3_sitearch}/samba/dcerpc/messaging.*.so
%{python3_sitearch}/samba/dcerpc/mdssvc.*.so
%{python3_sitearch}/samba/dcerpc/mgmt.*.so
%{python3_sitearch}/samba/dcerpc/misc.*.so
%{python3_sitearch}/samba/dcerpc/nbt.*.so
%{python3_sitearch}/samba/dcerpc/netlogon.*.so
%{python3_sitearch}/samba/dcerpc/ntlmssp.*.so
%{python3_sitearch}/samba/dcerpc/preg.*.so
%{python3_sitearch}/samba/dcerpc/samr.*.so
%{python3_sitearch}/samba/dcerpc/schannel.*.so
%{python3_sitearch}/samba/dcerpc/security.*.so
%{python3_sitearch}/samba/dcerpc/server_id.*.so
%{python3_sitearch}/samba/dcerpc/smb_acl.*.so
%{python3_sitearch}/samba/dcerpc/smb3posix.*.so
%{python3_sitearch}/samba/dcerpc/smbXsrv.*.so
%{python3_sitearch}/samba/dcerpc/spoolss.*.so
%{python3_sitearch}/samba/dcerpc/srvsvc.*.so
%{python3_sitearch}/samba/dcerpc/svcctl.*.so
%{python3_sitearch}/samba/dcerpc/unixinfo.*.so
%{python3_sitearch}/samba/dcerpc/winbind.*.so
%{python3_sitearch}/samba/dcerpc/windows_event_ids.*.so
%{python3_sitearch}/samba/dcerpc/winreg.*.so
%{python3_sitearch}/samba/dcerpc/winspool.*.so
%{python3_sitearch}/samba/dcerpc/witness.*.so
%{python3_sitearch}/samba/dcerpc/wkssvc.*.so
%{python3_sitearch}/samba/dcerpc/xattr.*.so
%{python3_sitearch}/samba/descriptor.py
%{python3_sitearch}/samba/dnsresolver.py
%dir %{python3_sitearch}/samba/domain
%{python3_sitearch}/samba/domain/__init__.py
%dir %{python3_sitearch}/samba/domain/__pycache__
%{python3_sitearch}/samba/domain/__pycache__/__init__.*.pyc
%dir %{python3_sitearch}/samba/domain/models
%{python3_sitearch}/samba/domain/models/__init__.py
%dir %{python3_sitearch}/samba/domain/models/__pycache__
%{python3_sitearch}/samba/domain/models/__pycache__/__init__.*.pyc
%{python3_sitearch}/samba/domain/models/__pycache__/auth_policy.*.pyc
%{python3_sitearch}/samba/domain/models/__pycache__/auth_silo.*.pyc
%{python3_sitearch}/samba/domain/models/__pycache__/claim_type.*.pyc
%{python3_sitearch}/samba/domain/models/__pycache__/computer.*.pyc
%{python3_sitearch}/samba/domain/models/__pycache__/constants.*.pyc
%{python3_sitearch}/samba/domain/models/__pycache__/container.*.pyc
%{python3_sitearch}/samba/domain/models/__pycache__/exceptions.*.pyc
%{python3_sitearch}/samba/domain/models/__pycache__/fields.*.pyc
%{python3_sitearch}/samba/domain/models/__pycache__/gmsa.*.pyc
%{python3_sitearch}/samba/domain/models/__pycache__/group.*.pyc
%{python3_sitearch}/samba/domain/models/__pycache__/model.*.pyc
%{python3_sitearch}/samba/domain/models/__pycache__/org.*.pyc
%{python3_sitearch}/samba/domain/models/__pycache__/person.*.pyc
%{python3_sitearch}/samba/domain/models/__pycache__/query.*.pyc
%{python3_sitearch}/samba/domain/models/__pycache__/registry.*.pyc
%{python3_sitearch}/samba/domain/models/__pycache__/schema.*.pyc
%{python3_sitearch}/samba/domain/models/__pycache__/site.*.pyc
%{python3_sitearch}/samba/domain/models/__pycache__/subnet.*.pyc
%{python3_sitearch}/samba/domain/models/__pycache__/types.*.pyc
%{python3_sitearch}/samba/domain/models/__pycache__/user.*.pyc
%{python3_sitearch}/samba/domain/models/__pycache__/value_type.*.pyc
%{python3_sitearch}/samba/domain/models/auth_policy.py
%{python3_sitearch}/samba/domain/models/auth_silo.py
%{python3_sitearch}/samba/domain/models/claim_type.py
%{python3_sitearch}/samba/domain/models/computer.py
%{python3_sitearch}/samba/domain/models/constants.py
%{python3_sitearch}/samba/domain/models/container.py
%{python3_sitearch}/samba/domain/models/exceptions.py
%{python3_sitearch}/samba/domain/models/fields.py
%{python3_sitearch}/samba/domain/models/gmsa.py
%{python3_sitearch}/samba/domain/models/group.py
%{python3_sitearch}/samba/domain/models/model.py
%{python3_sitearch}/samba/domain/models/org.py
%{python3_sitearch}/samba/domain/models/person.py
%{python3_sitearch}/samba/domain/models/query.py
%{python3_sitearch}/samba/domain/models/registry.py
%{python3_sitearch}/samba/domain/models/schema.py
%{python3_sitearch}/samba/domain/models/site.py
%{python3_sitearch}/samba/domain/models/subnet.py
%{python3_sitearch}/samba/domain/models/types.py
%{python3_sitearch}/samba/domain/models/user.py
%{python3_sitearch}/samba/domain/models/value_type.py
%{python3_sitearch}/samba/drs_utils.py
%{python3_sitearch}/samba/dsdb.*.so            
%{python3_sitearch}/samba/dsdb_dns.*.so
%{python3_sitearch}/samba/functional_level.py
%{python3_sitearch}/samba/gensec.*.so
%{python3_sitearch}/samba/getopt.py
%{python3_sitearch}/samba/gkdi.py
%{python3_sitearch}/samba/graph.py
%{python3_sitearch}/samba/hostconfig.py
%{python3_sitearch}/samba/idmap.py
%{python3_sitearch}/samba/join.py
%{python3_sitearch}/samba/lsa_utils.py
%{python3_sitearch}/samba/messaging.*.so
%{python3_sitearch}/samba/ndr.py
%{python3_sitearch}/samba/net.*.so
%{python3_sitearch}/samba/net_s3.*.so
%{python3_sitearch}/samba/ntstatus.*.so
%{python3_sitearch}/samba/posix_eadb.*.so
%dir %{python3_sitearch}/samba/emulate
%dir %{python3_sitearch}/samba/emulate/__pycache__
%{python3_sitearch}/samba/emulate/__pycache__/__init__.*.pyc
%{python3_sitearch}/samba/emulate/__pycache__/traffic.*.pyc
%{python3_sitearch}/samba/emulate/__pycache__/traffic_packets.*.pyc
%{python3_sitearch}/samba/emulate/__init__.py
%{python3_sitearch}/samba/emulate/traffic.py
%{python3_sitearch}/samba/emulate/traffic_packets.py
%dir %{python3_sitearch}/samba/gp
%dir %{python3_sitearch}/samba/gp/__pycache__
%{python3_sitearch}/samba/gp/__init__.py
%{python3_sitearch}/samba/gp/__pycache__/__init__.*.pyc
%{python3_sitearch}/samba/gp/__pycache__/gpclass.*.pyc
%{python3_sitearch}/samba/gp/__pycache__/gp_centrify_crontab_ext.*.pyc
%{python3_sitearch}/samba/gp/__pycache__/gp_centrify_sudoers_ext.*.pyc
%{python3_sitearch}/samba/gp/__pycache__/gp_cert_auto_enroll_ext.*.pyc
%{python3_sitearch}/samba/gp/__pycache__/gp_drive_maps_ext.*.pyc
%{python3_sitearch}/samba/gp/__pycache__/gp_chromium_ext.*.pyc
%{python3_sitearch}/samba/gp/__pycache__/gp_ext_loader.*.pyc
%{python3_sitearch}/samba/gp/__pycache__/gp_firefox_ext.*.pyc
%{python3_sitearch}/samba/gp/__pycache__/gp_firewalld_ext.*.pyc
%{python3_sitearch}/samba/gp/__pycache__/gp_gnome_settings_ext.*.pyc
%{python3_sitearch}/samba/gp/__pycache__/gp_msgs_ext.*.pyc
%{python3_sitearch}/samba/gp/__pycache__/gp_scripts_ext.*.pyc
%{python3_sitearch}/samba/gp/__pycache__/gp_sec_ext.*.pyc
%{python3_sitearch}/samba/gp/__pycache__/gp_smb_conf_ext.*.pyc
%{python3_sitearch}/samba/gp/__pycache__/gp_sudoers_ext.*.pyc
%{python3_sitearch}/samba/gp/__pycache__/vgp_access_ext.*.pyc
%{python3_sitearch}/samba/gp/__pycache__/vgp_files_ext.*.pyc
%{python3_sitearch}/samba/gp/__pycache__/vgp_issue_ext.*.pyc
%{python3_sitearch}/samba/gp/__pycache__/vgp_motd_ext.*.pyc
%{python3_sitearch}/samba/gp/__pycache__/vgp_openssh_ext.*.pyc
%{python3_sitearch}/samba/gp/__pycache__/vgp_startup_scripts_ext.*.pyc
%{python3_sitearch}/samba/gp/__pycache__/vgp_sudoers_ext.*.pyc
%{python3_sitearch}/samba/gp/__pycache__/vgp_symlink_ext.*.pyc
%{python3_sitearch}/samba/gp/gpclass.py
%{python3_sitearch}/samba/gp/gp_gnome_settings_ext.py
%{python3_sitearch}/samba/gp/gp_scripts_ext.py
%{python3_sitearch}/samba/gp/gp_sec_ext.py
%{python3_sitearch}/samba/gp/gp_centrify_crontab_ext.py
%{python3_sitearch}/samba/gp/gp_centrify_sudoers_ext.py
%{python3_sitearch}/samba/gp/gp_cert_auto_enroll_ext.py
%{python3_sitearch}/samba/gp/gp_drive_maps_ext.py
%{python3_sitearch}/samba/gp/gp_chromium_ext.py
%{python3_sitearch}/samba/gp/gp_ext_loader.py
%{python3_sitearch}/samba/gp/gp_firefox_ext.py
%{python3_sitearch}/samba/gp/gp_firewalld_ext.py
%{python3_sitearch}/samba/gp/gp_msgs_ext.py
%{python3_sitearch}/samba/gp/gp_smb_conf_ext.py
%{python3_sitearch}/samba/gp/gp_sudoers_ext.py
%dir %{python3_sitearch}/samba/gp/util
%dir %{python3_sitearch}/samba/gp/util/__pycache__
%{python3_sitearch}/samba/gp/util/__pycache__/logging.*.pyc
%{python3_sitearch}/samba/gp/util/logging.py
%{python3_sitearch}/samba/gp/vgp_access_ext.py
%{python3_sitearch}/samba/gp/vgp_files_ext.py
%{python3_sitearch}/samba/gp/vgp_issue_ext.py
%{python3_sitearch}/samba/gp/vgp_motd_ext.py
%{python3_sitearch}/samba/gp/vgp_openssh_ext.py
%{python3_sitearch}/samba/gp/vgp_startup_scripts_ext.py
%{python3_sitearch}/samba/gp/vgp_sudoers_ext.py
%{python3_sitearch}/samba/gp/vgp_symlink_ext.py
%{python3_sitearch}/samba/gpo.*.so
%dir %{python3_sitearch}/samba/gp_parse
%{python3_sitearch}/samba/gp_parse/__init__.py
%dir %{python3_sitearch}/samba/gp_parse/__pycache__
%{python3_sitearch}/samba/gp_parse/__pycache__/__init__.*.pyc
%{python3_sitearch}/samba/gp_parse/__pycache__/gp_aas.*.pyc
%{python3_sitearch}/samba/gp_parse/__pycache__/gp_csv.*.pyc
%{python3_sitearch}/samba/gp_parse/__pycache__/gp_inf.*.pyc
%{python3_sitearch}/samba/gp_parse/__pycache__/gp_ini.*.pyc
%{python3_sitearch}/samba/gp_parse/__pycache__/gp_pol.*.pyc
%{python3_sitearch}/samba/gp_parse/gp_aas.py
%{python3_sitearch}/samba/gp_parse/gp_csv.py
%{python3_sitearch}/samba/gp_parse/gp_inf.py
%{python3_sitearch}/samba/gp_parse/gp_ini.py
%{python3_sitearch}/samba/gp_parse/gp_pol.py
%{python3_sitearch}/samba/hresult.*.so
%{python3_sitearch}/samba/logger.py
%{python3_sitearch}/samba/mdb_util.py
%{python3_sitearch}/samba/ms_display_specifiers.py
%{python3_sitearch}/samba/ms_schema.py
%{python3_sitearch}/samba/netbios.*.so
%dir %{python3_sitearch}/samba/netcmd
%{python3_sitearch}/samba/netcmd/__init__.py
%dir %{python3_sitearch}/samba/netcmd/__pycache__
%{python3_sitearch}/samba/netcmd/__pycache__/__init__.*.pyc
%{python3_sitearch}/samba/netcmd/__pycache__/common.*.pyc
%{python3_sitearch}/samba/netcmd/__pycache__/computer.*.pyc
%{python3_sitearch}/samba/netcmd/__pycache__/contact.*.pyc
%{python3_sitearch}/samba/netcmd/__pycache__/dbcheck.*.pyc
%{python3_sitearch}/samba/netcmd/__pycache__/delegation.*.pyc
%{python3_sitearch}/samba/netcmd/__pycache__/dns.*.pyc
%{python3_sitearch}/samba/netcmd/__pycache__/drs.*.pyc
%{python3_sitearch}/samba/netcmd/__pycache__/dsacl.*.pyc
%{python3_sitearch}/samba/netcmd/__pycache__/encoders.*.pyc
%{python3_sitearch}/samba/netcmd/__pycache__/forest.*.pyc
%{python3_sitearch}/samba/netcmd/__pycache__/fsmo.*.pyc
%{python3_sitearch}/samba/netcmd/__pycache__/gpcommon.*.pyc
%{python3_sitearch}/samba/netcmd/__pycache__/gpo.*.pyc
%{python3_sitearch}/samba/netcmd/__pycache__/group.*.pyc
%{python3_sitearch}/samba/netcmd/__pycache__/ldapcmp.*.pyc
%{python3_sitearch}/samba/netcmd/__pycache__/main.*.pyc
%{python3_sitearch}/samba/netcmd/__pycache__/nettime.*.pyc
%{python3_sitearch}/samba/netcmd/__pycache__/ntacl.*.pyc
%{python3_sitearch}/samba/netcmd/__pycache__/ou.*.pyc
%{python3_sitearch}/samba/netcmd/__pycache__/processes.*.pyc
%{python3_sitearch}/samba/netcmd/__pycache__/pso.*.pyc
%{python3_sitearch}/samba/netcmd/__pycache__/rodc.*.pyc
%{python3_sitearch}/samba/netcmd/__pycache__/shell.*.pyc
%{python3_sitearch}/samba/netcmd/__pycache__/schema.*.pyc
%{python3_sitearch}/samba/netcmd/__pycache__/sites.*.pyc
%{python3_sitearch}/samba/netcmd/__pycache__/spn.*.pyc
%{python3_sitearch}/samba/netcmd/__pycache__/testparm.*.pyc
%{python3_sitearch}/samba/netcmd/__pycache__/validators.*.pyc
%{python3_sitearch}/samba/netcmd/__pycache__/visualize.*.pyc
%{python3_sitearch}/samba/netcmd/common.py
%{python3_sitearch}/samba/netcmd/computer.py
%{python3_sitearch}/samba/netcmd/contact.py
%{python3_sitearch}/samba/netcmd/dbcheck.py
%{python3_sitearch}/samba/netcmd/delegation.py
%dir %{python3_sitearch}/samba/netcmd/domain
%{python3_sitearch}/samba/netcmd/domain/__init__.py
%dir %{python3_sitearch}/samba/netcmd/domain/__pycache__
%{python3_sitearch}/samba/netcmd/domain/__pycache__/__init__.*.pyc
%{python3_sitearch}/samba/netcmd/domain/__pycache__/backup.*.pyc
%{python3_sitearch}/samba/netcmd/domain/__pycache__/classicupgrade.*.pyc
%{python3_sitearch}/samba/netcmd/domain/__pycache__/common.*.pyc
%{python3_sitearch}/samba/netcmd/domain/__pycache__/dcpromo.*.pyc
%{python3_sitearch}/samba/netcmd/domain/__pycache__/demote.*.pyc
%{python3_sitearch}/samba/netcmd/domain/__pycache__/functional_prep.*.pyc
%{python3_sitearch}/samba/netcmd/domain/__pycache__/info.*.pyc
%{python3_sitearch}/samba/netcmd/domain/__pycache__/join.*.pyc
%{python3_sitearch}/samba/netcmd/domain/__pycache__/keytab.*.pyc
%{python3_sitearch}/samba/netcmd/domain/__pycache__/leave.*.pyc
%{python3_sitearch}/samba/netcmd/domain/__pycache__/level.*.pyc
%{python3_sitearch}/samba/netcmd/domain/__pycache__/passwordsettings.*.pyc
%{python3_sitearch}/samba/netcmd/domain/__pycache__/provision.*.pyc
%{python3_sitearch}/samba/netcmd/domain/__pycache__/samba3upgrade.*.pyc
%{python3_sitearch}/samba/netcmd/domain/__pycache__/schemaupgrade.*.pyc
%{python3_sitearch}/samba/netcmd/domain/__pycache__/tombstones.*.pyc
%{python3_sitearch}/samba/netcmd/domain/__pycache__/trust.*.pyc
%dir %{python3_sitearch}/samba/netcmd/domain/auth
%{python3_sitearch}/samba/netcmd/domain/auth/__init__.py
%dir %{python3_sitearch}/samba/netcmd/domain/auth/__pycache__
%{python3_sitearch}/samba/netcmd/domain/auth/__pycache__/__init__.*.pyc
%dir %{python3_sitearch}/samba/netcmd/domain/auth/policy
%{python3_sitearch}/samba/netcmd/domain/auth/policy/__init__.py
%dir %{python3_sitearch}/samba/netcmd/domain/auth/policy/__pycache__
%{python3_sitearch}/samba/netcmd/domain/auth/policy/__pycache__/__init__.*.pyc
%{python3_sitearch}/samba/netcmd/domain/auth/policy/__pycache__/computer_allowed_to_authenticate_to.*.pyc
%{python3_sitearch}/samba/netcmd/domain/auth/policy/__pycache__/policy.*.pyc
%{python3_sitearch}/samba/netcmd/domain/auth/policy/__pycache__/service_allowed_to_authenticate_from.*.pyc
%{python3_sitearch}/samba/netcmd/domain/auth/policy/__pycache__/service_allowed_to_authenticate_to.*.pyc
%{python3_sitearch}/samba/netcmd/domain/auth/policy/__pycache__/user_allowed_to_authenticate_from.*.pyc
%{python3_sitearch}/samba/netcmd/domain/auth/policy/__pycache__/user_allowed_to_authenticate_to.*.pyc
%{python3_sitearch}/samba/netcmd/domain/auth/policy/computer_allowed_to_authenticate_to.py            
%{python3_sitearch}/samba/netcmd/domain/auth/policy/policy.py
%{python3_sitearch}/samba/netcmd/domain/auth/policy/service_allowed_to_authenticate_from.py
%{python3_sitearch}/samba/netcmd/domain/auth/policy/service_allowed_to_authenticate_to.py
%{python3_sitearch}/samba/netcmd/domain/auth/policy/user_allowed_to_authenticate_from.py
%{python3_sitearch}/samba/netcmd/domain/auth/policy/user_allowed_to_authenticate_to.py
%dir %{python3_sitearch}/samba/netcmd/domain/auth/silo
%{python3_sitearch}/samba/netcmd/domain/auth/silo/__init__.py
%dir %{python3_sitearch}/samba/netcmd/domain/auth/silo/__pycache__
%{python3_sitearch}/samba/netcmd/domain/auth/silo/__pycache__/__init__.*.pyc
%{python3_sitearch}/samba/netcmd/domain/auth/silo/__pycache__/member.*.pyc
%{python3_sitearch}/samba/netcmd/domain/auth/silo/__pycache__/silo.*.pyc
%{python3_sitearch}/samba/netcmd/domain/auth/silo/member.py
%{python3_sitearch}/samba/netcmd/domain/auth/silo/silo.py
%{python3_sitearch}/samba/netcmd/domain/backup.py
%dir %{python3_sitearch}/samba/netcmd/domain/claim
%{python3_sitearch}/samba/netcmd/domain/claim/__init__.py
%dir %{python3_sitearch}/samba/netcmd/domain/claim/__pycache__
%{python3_sitearch}/samba/netcmd/domain/claim/__pycache__/__init__.*.pyc
%{python3_sitearch}/samba/netcmd/domain/claim/__pycache__/claim_type.*.pyc
%{python3_sitearch}/samba/netcmd/domain/claim/__pycache__/value_type.*.pyc
%{python3_sitearch}/samba/netcmd/domain/claim/claim_type.py
%{python3_sitearch}/samba/netcmd/domain/claim/value_type.py
%{python3_sitearch}/samba/netcmd/domain/classicupgrade.py
%{python3_sitearch}/samba/netcmd/domain/common.py
%{python3_sitearch}/samba/netcmd/domain/dcpromo.py
%{python3_sitearch}/samba/netcmd/domain/demote.py
%{python3_sitearch}/samba/netcmd/domain/functional_prep.py
%{python3_sitearch}/samba/netcmd/domain/info.py
%{python3_sitearch}/samba/netcmd/domain/join.py
%dir %{python3_sitearch}/samba/netcmd/domain/kds
%{python3_sitearch}/samba/netcmd/domain/kds/__init__.py
%dir %{python3_sitearch}/samba/netcmd/domain/kds/__pycache__
%{python3_sitearch}/samba/netcmd/domain/kds/__pycache__/__init__.*.pyc
%{python3_sitearch}/samba/netcmd/domain/kds/__pycache__/root_key.*.pyc
%{python3_sitearch}/samba/netcmd/domain/kds/root_key.py
%{python3_sitearch}/samba/netcmd/domain/keytab.py
%{python3_sitearch}/samba/netcmd/domain/leave.py
%{python3_sitearch}/samba/netcmd/domain/level.py
%{python3_sitearch}/samba/netcmd/domain/passwordsettings.py
%{python3_sitearch}/samba/netcmd/domain/provision.py
%{python3_sitearch}/samba/netcmd/domain/samba3upgrade.py
%{python3_sitearch}/samba/netcmd/domain/schemaupgrade.py
%{python3_sitearch}/samba/netcmd/domain/tombstones.py
%{python3_sitearch}/samba/netcmd/domain/trust.py
%{python3_sitearch}/samba/netcmd/dns.py
%{python3_sitearch}/samba/netcmd/drs.py
%{python3_sitearch}/samba/netcmd/dsacl.py
%{python3_sitearch}/samba/netcmd/encoders.py
%{python3_sitearch}/samba/netcmd/forest.py
%{python3_sitearch}/samba/netcmd/fsmo.py
%{python3_sitearch}/samba/netcmd/gpcommon.py
%{python3_sitearch}/samba/netcmd/gpo.py
%{python3_sitearch}/samba/netcmd/group.py
%{python3_sitearch}/samba/netcmd/ldapcmp.py
%{python3_sitearch}/samba/netcmd/main.py
%{python3_sitearch}/samba/netcmd/nettime.py
%{python3_sitearch}/samba/netcmd/ntacl.py
%{python3_sitearch}/samba/netcmd/ou.py
%{python3_sitearch}/samba/netcmd/processes.py
%{python3_sitearch}/samba/netcmd/pso.py
%{python3_sitearch}/samba/netcmd/rodc.py
%{python3_sitearch}/samba/netcmd/schema.py
%dir %{python3_sitearch}/samba/netcmd/service_account
%{python3_sitearch}/samba/netcmd/service_account/__init__.py
%dir %{python3_sitearch}/samba/netcmd/service_account/__pycache__
%{python3_sitearch}/samba/netcmd/service_account/__pycache__/__init__.*.pyc
%{python3_sitearch}/samba/netcmd/service_account/__pycache__/group_msa_membership.*.pyc
%{python3_sitearch}/samba/netcmd/service_account/__pycache__/service_account.*.pyc
%{python3_sitearch}/samba/netcmd/service_account/group_msa_membership.py
%{python3_sitearch}/samba/netcmd/service_account/service_account.py
%{python3_sitearch}/samba/netcmd/shell.py
%{python3_sitearch}/samba/netcmd/sites.py
%{python3_sitearch}/samba/netcmd/spn.py
%{python3_sitearch}/samba/netcmd/testparm.py
%dir %{python3_sitearch}/samba/netcmd/user
%{python3_sitearch}/samba/netcmd/user/__init__.py
%{python3_sitearch}/samba/netcmd/user/add.py
%{python3_sitearch}/samba/netcmd/user/add_unix_attrs.py
%dir %{python3_sitearch}/samba/netcmd/user/auth
%{python3_sitearch}/samba/netcmd/user/auth/__init__.py
%{python3_sitearch}/samba/netcmd/user/auth/policy.py
%dir %{python3_sitearch}/samba/netcmd/user/auth/__pycache__
%{python3_sitearch}/samba/netcmd/user/auth/__pycache__/__init__.*.pyc
%{python3_sitearch}/samba/netcmd/user/auth/__pycache__/policy.*.pyc
%{python3_sitearch}/samba/netcmd/user/auth/__pycache__/silo.*.pyc
%{python3_sitearch}/samba/netcmd/user/auth/silo.py
%{python3_sitearch}/samba/netcmd/user/delete.py
%{python3_sitearch}/samba/netcmd/user/disable.py
%{python3_sitearch}/samba/netcmd/user/edit.py
%{python3_sitearch}/samba/netcmd/user/enable.py
%{python3_sitearch}/samba/netcmd/user/getgroups.py
%{python3_sitearch}/samba/netcmd/user/list.py
%{python3_sitearch}/samba/netcmd/user/move.py
%{python3_sitearch}/samba/netcmd/user/password.py
%dir %{python3_sitearch}/samba/netcmd/user/__pycache__
%{python3_sitearch}/samba/netcmd/user/__pycache__/__init__.*.pyc
%{python3_sitearch}/samba/netcmd/user/__pycache__/add.*.pyc
%{python3_sitearch}/samba/netcmd/user/__pycache__/add_unix_attrs.*.pyc
%{python3_sitearch}/samba/netcmd/user/__pycache__/delete.*.pyc
%{python3_sitearch}/samba/netcmd/user/__pycache__/disable.*.pyc
%{python3_sitearch}/samba/netcmd/user/__pycache__/edit.*.pyc
%{python3_sitearch}/samba/netcmd/user/__pycache__/enable.*.pyc
%{python3_sitearch}/samba/netcmd/user/__pycache__/getgroups.*.pyc
%{python3_sitearch}/samba/netcmd/user/__pycache__/list.*.pyc
%{python3_sitearch}/samba/netcmd/user/__pycache__/move.*.pyc
%{python3_sitearch}/samba/netcmd/user/__pycache__/password.*.pyc
%{python3_sitearch}/samba/netcmd/user/__pycache__/rename.*.pyc
%{python3_sitearch}/samba/netcmd/user/__pycache__/sensitive.*.pyc
%{python3_sitearch}/samba/netcmd/user/__pycache__/setexpiry.*.pyc
%{python3_sitearch}/samba/netcmd/user/__pycache__/setpassword.*.pyc
%{python3_sitearch}/samba/netcmd/user/__pycache__/setprimarygroup.*.pyc
%{python3_sitearch}/samba/netcmd/user/__pycache__/unlock.*.pyc
%dir %{python3_sitearch}/samba/netcmd/user/readpasswords
%{python3_sitearch}/samba/netcmd/user/readpasswords/common.py
%{python3_sitearch}/samba/netcmd/user/readpasswords/get_kerberos_ticket.py
%{python3_sitearch}/samba/netcmd/user/readpasswords/getpassword.py
%{python3_sitearch}/samba/netcmd/user/readpasswords/__init__.py
%dir %{python3_sitearch}/samba/netcmd/user/readpasswords/__pycache__
%{python3_sitearch}/samba/netcmd/user/readpasswords/__pycache__/__init__.*.pyc
%{python3_sitearch}/samba/netcmd/user/readpasswords/__pycache__/common.*.pyc
%{python3_sitearch}/samba/netcmd/user/readpasswords/__pycache__/get_kerberos_ticket.*.pyc
%{python3_sitearch}/samba/netcmd/user/readpasswords/__pycache__/getpassword.*.pyc
%{python3_sitearch}/samba/netcmd/user/readpasswords/__pycache__/show.*.pyc
%{python3_sitearch}/samba/netcmd/user/readpasswords/__pycache__/syncpasswords.*.pyc
%{python3_sitearch}/samba/netcmd/user/readpasswords/show.py
%{python3_sitearch}/samba/netcmd/user/readpasswords/syncpasswords.py
%{python3_sitearch}/samba/netcmd/user/rename.py
%{python3_sitearch}/samba/netcmd/user/sensitive.py
%{python3_sitearch}/samba/netcmd/user/setexpiry.py
%{python3_sitearch}/samba/netcmd/user/setpassword.py
%{python3_sitearch}/samba/netcmd/user/setprimarygroup.py
%{python3_sitearch}/samba/netcmd/user/unlock.py
%{python3_sitearch}/samba/netcmd/validators.py
%{python3_sitearch}/samba/netcmd/visualize.py
%{python3_sitearch}/samba/ntacls.py
%{python3_sitearch}/samba/nt_time.py
%{python3_sitearch}/samba/param.*.so
%{python3_sitearch}/samba/policies.py
%{python3_sitearch}/samba/policy.*.so
%{python3_sitearch}/samba/registry.*.so
%{python3_sitearch}/samba/reparse_symlink.*.so
%{python3_sitearch}/samba/security.*.so
%{python3_sitearch}/samba/safe_tarfile.py
%dir %{python3_sitearch}/samba/samba3
%{python3_sitearch}/samba/samba3/__init__.py
%dir %{python3_sitearch}/samba/samba3/__pycache__
%{python3_sitearch}/samba/samba3/__pycache__/__init__.*.pyc
%{python3_sitearch}/samba/samba3/__pycache__/libsmb_samba_internal.*.pyc            
%{python3_sitearch}/samba/samba3/libsmb_samba_cwrapper.cpython*.so            
%{python3_sitearch}/samba/samba3/libsmb_samba_internal.py
%{python3_sitearch}/samba/samba3/mdscli.*.so
%{python3_sitearch}/samba/samba3/param.*.so
%{python3_sitearch}/samba/samba3/passdb.*.so
%{python3_sitearch}/samba/samba3/smbconf.*.so
%{python3_sitearch}/samba/samba3/smbd.*.so
%{python3_sitearch}/samba/sd_utils.py
%{python3_sitearch}/samba/sites.py
%{python3_sitearch}/samba/smbconf.*.so
%{python3_sitearch}/samba/subnets.py
%dir %{python3_sitearch}/samba/subunit
%{python3_sitearch}/samba/subunit/__init__.py
%dir %{python3_sitearch}/samba/subunit/__pycache__
%{python3_sitearch}/samba/subunit/__pycache__/__init__.*.pyc
%{python3_sitearch}/samba/subunit/__pycache__/run.*.pyc
%{python3_sitearch}/samba/subunit/run.py
%{python3_sitearch}/samba/tdb_util.py
%{python3_sitearch}/samba/upgrade.py
%{python3_sitearch}/samba/upgradehelpers.py
%{python3_sitearch}/samba/werror.*.so
%{python3_sitearch}/samba/xattr.py
%{python3_sitearch}/samba/xattr_native.*.so
%{python3_sitearch}/samba/xattr_tdb.*.so

%{_libdir}/samba/libsamba-net-join.cpython*.so
%{_libdir}/samba/libsamba-python.cpython*.so
%config(noreplace) /etc/ld.so.conf.d/*


%if %{with_dc}
%files -n python3-%{name}-dc
%{python3_sitearch}/samba/samdb.py
%{python3_sitearch}/samba/schema.py

%{python3_sitearch}/samba/__pycache__/domain_update.*.pyc
%{python3_sitearch}/samba/__pycache__/dnsserver.*.pyc
%{python3_sitearch}/samba/__pycache__/forest_update.*.pyc
%{python3_sitearch}/samba/__pycache__/ms_forest_updates_markdown.*.pyc
%{python3_sitearch}/samba/__pycache__/ms_schema_markdown.*.pyc
%{python3_sitearch}/samba/__pycache__/remove_dc.*.pyc
%{python3_sitearch}/samba/__pycache__/samdb.*.pyc
%{python3_sitearch}/samba/__pycache__/schema.*.pyc
%{python3_sitearch}/samba/__pycache__/uptodateness.*.pyc

%{python3_sitearch}/samba/dcerpc/dnsserver.*.so
%{python3_sitearch}/samba/dckeytab.*.so
%{python3_sitearch}/samba/domain_update.py
%{python3_sitearch}/samba/forest_update.py
%{python3_sitearch}/samba/ms_forest_updates_markdown.py
%{python3_sitearch}/samba/ms_schema_markdown.py

%dir %{python3_sitearch}/samba/kcc
%{python3_sitearch}/samba/kcc/__init__.py
%{python3_sitearch}/samba/kcc/debug.py
%{python3_sitearch}/samba/kcc/graph.py
%{python3_sitearch}/samba/kcc/graph_utils.py
%{python3_sitearch}/samba/kcc/kcc_utils.py
%{python3_sitearch}/samba/kcc/ldif_import_export.py
%{python3_sitearch}/samba/dnsserver.py

%dir %{python3_sitearch}/samba/kcc/__pycache__
%{python3_sitearch}/samba/kcc/__pycache__/__init__.*.pyc
%{python3_sitearch}/samba/kcc/__pycache__/debug.*.pyc
%{python3_sitearch}/samba/kcc/__pycache__/graph.*.pyc
%{python3_sitearch}/samba/kcc/__pycache__/graph_utils.*.pyc
%{python3_sitearch}/samba/kcc/__pycache__/kcc_utils.*.pyc
%{python3_sitearch}/samba/kcc/__pycache__/ldif_import_export.*.pyc

%dir %{python3_sitearch}/samba/provision
%{python3_sitearch}/samba/provision/backend.py
%{python3_sitearch}/samba/provision/common.py
%{python3_sitearch}/samba/provision/kerberos.py
%{python3_sitearch}/samba/provision/kerberos_implementation.py
%{python3_sitearch}/samba/provision/sambadns.py

%dir %{python3_sitearch}/samba/provision/__pycache__
%{python3_sitearch}/samba/provision/__init__.py
%{python3_sitearch}/samba/provision/__pycache__/__init__.*.pyc
%{python3_sitearch}/samba/provision/__pycache__/backend.*.pyc
%{python3_sitearch}/samba/provision/__pycache__/common.*.pyc
%{python3_sitearch}/samba/provision/__pycache__/kerberos.*.pyc
%{python3_sitearch}/samba/provision/__pycache__/kerberos_implementation.*.pyc
%{python3_sitearch}/samba/provision/__pycache__/sambadns.*.pyc

%{python3_sitearch}/samba/remove_dc.py
%{python3_sitearch}/samba/uptodateness.py
%config(noreplace) /etc/ld.so.conf.d/*
%endif

%files -n python3-%{name}-test
%dir %{python3_sitearch}/samba/tests
%{python3_sitearch}/samba/tests/__init__.py
%dir %{python3_sitearch}/samba/tests/__pycache__
%{python3_sitearch}/samba/tests/__pycache__/__init__.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/audit_log_base.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/audit_log_dsdb.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/audit_log_pass_change.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/auth.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/auth_log.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/auth_log_base.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/auth_log_pass_change.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/auth_log_ncalrpc.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/auth_log_netlogon.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/auth_log_netlogon_bad_creds.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/auth_log_samlogon.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/auth_log_winbind.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/common.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/complex_expressions.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/compression.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/conditional_ace_assembler.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/conditional_ace_bytes.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/conditional_ace_claims.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/core.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/credentials.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/cred_opt.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/dckeytab.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/dns.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/dns_aging.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/dns_base.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/dns_forwarder.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/dns_invalid.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/dns_packet.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/dns_tkey.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/dns_wildcard.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/dsdb.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/dsdb_api.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/dsdb_dns.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/dsdb_lock.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/dsdb_quiet_env_tests.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/dsdb_quiet_provision_tests.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/dsdb_schema_attributes.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/docs.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/domain_backup.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/domain_backup_offline.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/encrypted_secrets.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/gensec.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/get_opt.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/getdcname.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/gkdi.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/glue.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/gpo.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/gpo_member.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/graph.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/group_audit.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/hostconfig.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/imports.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/join.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/krb5_credentials.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/ldap_raw.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/ldap_referrals.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/ldap_spn.*.pyc            
%{python3_sitearch}/samba/tests/__pycache__/ldap_upn_sam_account.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/ldap_whoami.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/loadparm.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/logfiles.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/libsmb.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/libsmb-basic.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/lsa_string.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/messaging.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/netbios.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/netcmd.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/net_join_no_spnego.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/net_join.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/netlogonsvc.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/ntacls.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/ntacls_backup.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/ntlmdisabled.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/ntlm_auth.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/ntlm_auth_base.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/ntlm_auth_krb5.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/pam_winbind.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/pam_winbind_chauthtok.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/pam_winbind_setcred.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/pam_winbind_warn_pwd_expire.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/param.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/password_hash.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/password_hash_fl2003.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/password_hash_fl2008.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/password_hash_gpgme.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/password_hash_ldap.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/password_quality.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/password_test.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/policy.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/posixacl.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/prefork_restart.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/process_limits.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/provision.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/pso.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/py_credentials.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/registry.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/reparsepoints.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/rust.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/s3idmapdb.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/s3param.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/s3passdb.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/s3registry.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/s3windb.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/s3_net_join.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/safe_tarfile.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/samba3sam.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/samba_upgradedns_lmdb.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/samba_startup_fl_change.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/samdb.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/samdb_api.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/sddl.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/sddl_conditional_ace.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/security.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/security_descriptors.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/segfault.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/sid_strings.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/smb.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/smb1posix.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/smb2symlink.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/smb3unix.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/smbconf.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/smb-notify.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/smbd_base.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/smbd_fuzztest.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/source.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/source_chars.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/strings.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/subunitrun.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/tdb_util.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/token_factory.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/upgrade.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/upgradeprovision.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/upgradeprovisionneeddc.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/usage.*.pyc
%{python3_sitearch}/samba/tests/__pycache__/xattr.*.pyc
%{python3_sitearch}/samba/tests/audit_log_base.py
%{python3_sitearch}/samba/tests/audit_log_dsdb.py
%{python3_sitearch}/samba/tests/audit_log_pass_change.py
%{python3_sitearch}/samba/tests/auth.py
%{python3_sitearch}/samba/tests/auth_log.py
%{python3_sitearch}/samba/tests/auth_log_base.py
%{python3_sitearch}/samba/tests/auth_log_ncalrpc.py
%{python3_sitearch}/samba/tests/auth_log_netlogon_bad_creds.py
%{python3_sitearch}/samba/tests/auth_log_netlogon.py
%{python3_sitearch}/samba/tests/auth_log_pass_change.py
%{python3_sitearch}/samba/tests/auth_log_samlogon.py
%{python3_sitearch}/samba/tests/auth_log_winbind.py
%dir %{python3_sitearch}/samba/tests/blackbox
%{python3_sitearch}/samba/tests/blackbox/__init__.py
%dir %{python3_sitearch}/samba/tests/blackbox/__pycache__
%{python3_sitearch}/samba/tests/blackbox/__pycache__/__init__.*.pyc
%{python3_sitearch}/samba/tests/blackbox/__pycache__/bug13653.*.pyc
%{python3_sitearch}/samba/tests/blackbox/__pycache__/check_output.*.pyc
%{python3_sitearch}/samba/tests/blackbox/__pycache__/claims.*.pyc
%{python3_sitearch}/samba/tests/blackbox/__pycache__/downgradedatabase.*.pyc
%{python3_sitearch}/samba/tests/blackbox/__pycache__/gmsa.*.pyc
%{python3_sitearch}/samba/tests/blackbox/__pycache__/http_chunk.*.pyc
%{python3_sitearch}/samba/tests/blackbox/__pycache__/http_content.*.pyc
%{python3_sitearch}/samba/tests/blackbox/__pycache__/mdsearch.*.pyc
%{python3_sitearch}/samba/tests/blackbox/__pycache__/misc_dfs_widelink.*.pyc
%{python3_sitearch}/samba/tests/blackbox/__pycache__/ndrdump.*.pyc
%{python3_sitearch}/samba/tests/blackbox/__pycache__/netads_dns.*.pyc
%{python3_sitearch}/samba/tests/blackbox/__pycache__/netads_json.*.pyc
%{python3_sitearch}/samba/tests/blackbox/__pycache__/rpcd_witness_samba_only.*.pyc
%{python3_sitearch}/samba/tests/blackbox/__pycache__/samba_dnsupdate.*.pyc
%{python3_sitearch}/samba/tests/blackbox/__pycache__/smbcacls.*.pyc
%{python3_sitearch}/samba/tests/blackbox/__pycache__/smbcacls_basic.*.pyc
%{python3_sitearch}/samba/tests/blackbox/__pycache__/smbcacls_dfs_propagate_inherit.*.pyc
%{python3_sitearch}/samba/tests/blackbox/__pycache__/smbcacls_propagate_inhertance.*.pyc
%{python3_sitearch}/samba/tests/blackbox/__pycache__/smbcacls_save_restore.*.pyc
%{python3_sitearch}/samba/tests/blackbox/__pycache__/smbcontrol.*.pyc
%{python3_sitearch}/samba/tests/blackbox/__pycache__/smbcontrol_process.*.pyc
%{python3_sitearch}/samba/tests/blackbox/__pycache__/traffic_learner.*.pyc
%{python3_sitearch}/samba/tests/blackbox/__pycache__/traffic_replay.*.pyc
%{python3_sitearch}/samba/tests/blackbox/__pycache__/traffic_summary.*.pyc
%{python3_sitearch}/samba/tests/blackbox/bug13653.py
%{python3_sitearch}/samba/tests/blackbox/check_output.py
%{python3_sitearch}/samba/tests/blackbox/claims.py
%{python3_sitearch}/samba/tests/blackbox/downgradedatabase.py
%{python3_sitearch}/samba/tests/blackbox/gmsa.py
%{python3_sitearch}/samba/tests/blackbox/http_chunk.py
%{python3_sitearch}/samba/tests/blackbox/http_content.py
%{python3_sitearch}/samba/tests/blackbox/mdsearch.py
%{python3_sitearch}/samba/tests/blackbox/misc_dfs_widelink.py
%{python3_sitearch}/samba/tests/blackbox/ndrdump.py
%{python3_sitearch}/samba/tests/blackbox/netads_dns.py
%{python3_sitearch}/samba/tests/blackbox/netads_json.py
%{python3_sitearch}/samba/tests/blackbox/rpcd_witness_samba_only.py
%{python3_sitearch}/samba/tests/blackbox/samba_dnsupdate.py
%{python3_sitearch}/samba/tests/blackbox/smbcacls.py
%{python3_sitearch}/samba/tests/blackbox/smbcacls_basic.py
%{python3_sitearch}/samba/tests/blackbox/smbcacls_dfs_propagate_inherit.py
%{python3_sitearch}/samba/tests/blackbox/smbcacls_propagate_inhertance.py
%{python3_sitearch}/samba/tests/blackbox/smbcacls_save_restore.py
%{python3_sitearch}/samba/tests/blackbox/smbcontrol.py
%{python3_sitearch}/samba/tests/blackbox/smbcontrol_process.py
%{python3_sitearch}/samba/tests/blackbox/traffic_learner.py
%{python3_sitearch}/samba/tests/blackbox/traffic_replay.py
%{python3_sitearch}/samba/tests/blackbox/traffic_summary.py
%{python3_sitearch}/samba/tests/common.py
%{python3_sitearch}/samba/tests/compression.py
%{python3_sitearch}/samba/tests/complex_expressions.py
%{python3_sitearch}/samba/tests/conditional_ace_assembler.py
%{python3_sitearch}/samba/tests/conditional_ace_bytes.py
%{python3_sitearch}/samba/tests/conditional_ace_claims.py
%{python3_sitearch}/samba/tests/core.py
%{python3_sitearch}/samba/tests/credentials.py
%{python3_sitearch}/samba/tests/cred_opt.py
%dir %{python3_sitearch}/samba/tests/dcerpc
%{python3_sitearch}/samba/tests/dcerpc/__init__.py
%dir %{python3_sitearch}/samba/tests/dcerpc/__pycache__
%{python3_sitearch}/samba/tests/dcerpc/__pycache__/__init__.*.pyc
%{python3_sitearch}/samba/tests/dcerpc/__pycache__/array.*.pyc
%{python3_sitearch}/samba/tests/dcerpc/__pycache__/bare.*.pyc
%{python3_sitearch}/samba/tests/dcerpc/__pycache__/binding.*.pyc
%{python3_sitearch}/samba/tests/dcerpc/__pycache__/dnsserver.*.pyc
%{python3_sitearch}/samba/tests/dcerpc/__pycache__/integer.*.pyc
%{python3_sitearch}/samba/tests/dcerpc/__pycache__/lsa.*.pyc
%{python3_sitearch}/samba/tests/dcerpc/__pycache__/lsa_utils.*.pyc
%{python3_sitearch}/samba/tests/dcerpc/__pycache__/mdssvc.*.pyc
%{python3_sitearch}/samba/tests/dcerpc/__pycache__/misc.*.pyc
%{python3_sitearch}/samba/tests/dcerpc/__pycache__/raw_protocol.*.pyc
%{python3_sitearch}/samba/tests/dcerpc/__pycache__/raw_testcase.*.pyc
%{python3_sitearch}/samba/tests/dcerpc/__pycache__/registry.*.pyc
%{python3_sitearch}/samba/tests/dcerpc/__pycache__/rpc_talloc.*.pyc
%{python3_sitearch}/samba/tests/dcerpc/__pycache__/rpcecho.*.pyc
%{python3_sitearch}/samba/tests/dcerpc/__pycache__/sam.*.pyc
%{python3_sitearch}/samba/tests/dcerpc/__pycache__/samr_change_password.*.pyc
%{python3_sitearch}/samba/tests/dcerpc/__pycache__/srvsvc.*.pyc
%{python3_sitearch}/samba/tests/dcerpc/__pycache__/string_tests.*.pyc
%{python3_sitearch}/samba/tests/dcerpc/__pycache__/testrpc.*.pyc
%{python3_sitearch}/samba/tests/dcerpc/__pycache__/unix.*.pyc
%{python3_sitearch}/samba/tests/dcerpc/array.py
%{python3_sitearch}/samba/tests/dcerpc/bare.py
%{python3_sitearch}/samba/tests/dcerpc/binding.py
%{python3_sitearch}/samba/tests/dcerpc/dnsserver.py
%{python3_sitearch}/samba/tests/dcerpc/integer.py
%{python3_sitearch}/samba/tests/dcerpc/lsa.py
%{python3_sitearch}/samba/tests/dcerpc/lsa_utils.py
%{python3_sitearch}/samba/tests/dcerpc/mdssvc.py
%{python3_sitearch}/samba/tests/dcerpc/misc.py
%{python3_sitearch}/samba/tests/dcerpc/raw_protocol.py
%{python3_sitearch}/samba/tests/dcerpc/raw_testcase.py
%{python3_sitearch}/samba/tests/dcerpc/registry.py
%{python3_sitearch}/samba/tests/dcerpc/rpc_talloc.py
%{python3_sitearch}/samba/tests/dcerpc/rpcecho.py
%{python3_sitearch}/samba/tests/dcerpc/sam.py
%{python3_sitearch}/samba/tests/dcerpc/samr_change_password.py
%{python3_sitearch}/samba/tests/dcerpc/srvsvc.py
%{python3_sitearch}/samba/tests/dcerpc/string_tests.py
%{python3_sitearch}/samba/tests/dcerpc/testrpc.py
%{python3_sitearch}/samba/tests/dcerpc/unix.py
%{python3_sitearch}/samba/tests/dckeytab.py
%{python3_sitearch}/samba/tests/dns.py
%{python3_sitearch}/samba/tests/dns_aging.py
%{python3_sitearch}/samba/tests/dns_base.py
%{python3_sitearch}/samba/tests/dns_forwarder.py
%dir %{python3_sitearch}/samba/tests/dns_forwarder_helpers
%{python3_sitearch}/samba/tests/dns_forwarder_helpers/__pycache__/server.*.pyc
%{python3_sitearch}/samba/tests/dns_forwarder_helpers/server.py
%{python3_sitearch}/samba/tests/dns_invalid.py
%{python3_sitearch}/samba/tests/dns_packet.py
%{python3_sitearch}/samba/tests/dns_tkey.py
%{python3_sitearch}/samba/tests/dns_wildcard.py
%{python3_sitearch}/samba/tests/dsdb.py
%{python3_sitearch}/samba/tests/dsdb_api.py
%{python3_sitearch}/samba/tests/dsdb_dns.py
%{python3_sitearch}/samba/tests/dsdb_lock.py
%{python3_sitearch}/samba/tests/dsdb_schema_attributes.py
%{python3_sitearch}/samba/tests/dsdb_quiet_env_tests.py
%{python3_sitearch}/samba/tests/dsdb_quiet_provision_tests.py
%{python3_sitearch}/samba/tests/docs.py
%{python3_sitearch}/samba/tests/domain_backup.py
%{python3_sitearch}/samba/tests/domain_backup_offline.py
%dir %{python3_sitearch}/samba/tests/emulate
%{python3_sitearch}/samba/tests/emulate/__init__.py
%dir %{python3_sitearch}/samba/tests/emulate/__pycache__
%{python3_sitearch}/samba/tests/emulate/__pycache__/__init__.*.pyc
%{python3_sitearch}/samba/tests/emulate/__pycache__/traffic.*.pyc
%{python3_sitearch}/samba/tests/emulate/__pycache__/traffic_packet.*.pyc
%{python3_sitearch}/samba/tests/emulate/traffic.py
%{python3_sitearch}/samba/tests/emulate/traffic_packet.py
%{python3_sitearch}/samba/tests/encrypted_secrets.py
%{python3_sitearch}/samba/tests/gensec.py
%{python3_sitearch}/samba/tests/getdcname.py
%{python3_sitearch}/samba/tests/get_opt.py
%{python3_sitearch}/samba/tests/gkdi.py
%{python3_sitearch}/samba/tests/glue.py
%{python3_sitearch}/samba/tests/gpo.py
%{python3_sitearch}/samba/tests/gpo_member.py
%{python3_sitearch}/samba/tests/graph.py
%{python3_sitearch}/samba/tests/group_audit.py
%{python3_sitearch}/samba/tests/hostconfig.py
%{python3_sitearch}/samba/tests/imports.py
%{python3_sitearch}/samba/tests/join.py
%dir %{python3_sitearch}/samba/tests/kcc
%{python3_sitearch}/samba/tests/kcc/__init__.py
%dir %{python3_sitearch}/samba/tests/kcc/__pycache__
%{python3_sitearch}/samba/tests/kcc/__pycache__/__init__.*.pyc
%{python3_sitearch}/samba/tests/kcc/__pycache__/graph.*.pyc
%{python3_sitearch}/samba/tests/kcc/__pycache__/graph_utils.*.pyc
%{python3_sitearch}/samba/tests/kcc/__pycache__/kcc_utils.*.pyc
%{python3_sitearch}/samba/tests/kcc/__pycache__/ldif_import_export.*.pyc
%{python3_sitearch}/samba/tests/kcc/graph.py
%{python3_sitearch}/samba/tests/kcc/graph_utils.py
%{python3_sitearch}/samba/tests/kcc/kcc_utils.py
%{python3_sitearch}/samba/tests/kcc/ldif_import_export.py
%dir %{python3_sitearch}/samba/tests/krb5
%dir %{python3_sitearch}/samba/tests/krb5/__pycache__
%{python3_sitearch}/samba/tests/krb5/__pycache__/alias_tests.*.pyc
%{python3_sitearch}/samba/tests/krb5/__pycache__/as_canonicalization_tests.*.pyc
%{python3_sitearch}/samba/tests/krb5/__pycache__/as_req_tests.*.pyc
%{python3_sitearch}/samba/tests/krb5/__pycache__/authn_policy_tests.*.pyc
%{python3_sitearch}/samba/tests/krb5/__pycache__/claims_in_pac.*.pyc
%{python3_sitearch}/samba/tests/krb5/__pycache__/claims_tests.*.pyc
%{python3_sitearch}/samba/tests/krb5/__pycache__/compatability_tests.*.pyc
%{python3_sitearch}/samba/tests/krb5/__pycache__/conditional_ace_tests.*.pyc
%{python3_sitearch}/samba/tests/krb5/__pycache__/device_tests.*.pyc
%{python3_sitearch}/samba/tests/krb5/__pycache__/etype_tests.*.pyc
%{python3_sitearch}/samba/tests/krb5/__pycache__/fast_tests.*.pyc
%{python3_sitearch}/samba/tests/krb5/__pycache__/gkdi_tests.*.pyc
%{python3_sitearch}/samba/tests/krb5/__pycache__/gmsa_tests.*.pyc
%{python3_sitearch}/samba/tests/krb5/__pycache__/group_tests.*.pyc
%{python3_sitearch}/samba/tests/krb5/__pycache__/kcrypto.*.pyc
%{python3_sitearch}/samba/tests/krb5/__pycache__/kdc_base_test.*.pyc
%{python3_sitearch}/samba/tests/krb5/__pycache__/kdc_tests.*.pyc
%{python3_sitearch}/samba/tests/krb5/__pycache__/kdc_tgs_tests.*.pyc
%{python3_sitearch}/samba/tests/krb5/__pycache__/kdc_tgt_tests.*.pyc
%{python3_sitearch}/samba/tests/krb5/__pycache__/kpasswd_tests.*.pyc
%{python3_sitearch}/samba/tests/krb5/__pycache__/lockout_tests.*.pyc
%{python3_sitearch}/samba/tests/krb5/__pycache__/ms_kile_client_principal_lookup_tests.*.pyc
%{python3_sitearch}/samba/tests/krb5/__pycache__/netlogon.*.pyc
%{python3_sitearch}/samba/tests/krb5/__pycache__/nt_hash_tests.*.pyc
%{python3_sitearch}/samba/tests/krb5/__pycache__/pac_align_tests.*.pyc
%{python3_sitearch}/samba/tests/krb5/__pycache__/pkinit_tests.*.pyc
%{python3_sitearch}/samba/tests/krb5/__pycache__/protected_users_tests.*.pyc
%{python3_sitearch}/samba/tests/krb5/__pycache__/raw_testcase.*.pyc
%{python3_sitearch}/samba/tests/krb5/__pycache__/rfc4120_constants.*.pyc
%{python3_sitearch}/samba/tests/krb5/__pycache__/rfc4120_pyasn1.*.pyc
%{python3_sitearch}/samba/tests/krb5/__pycache__/rfc4120_pyasn1_generated.*.pyc
%{python3_sitearch}/samba/tests/krb5/__pycache__/rodc_tests.*.pyc
%{python3_sitearch}/samba/tests/krb5/__pycache__/salt_tests.*.pyc
%{python3_sitearch}/samba/tests/krb5/__pycache__/simple_tests.*.pyc
%{python3_sitearch}/samba/tests/krb5/__pycache__/spn_tests.*.pyc
%{python3_sitearch}/samba/tests/krb5/__pycache__/s4u_tests.*.pyc
%{python3_sitearch}/samba/tests/krb5/__pycache__/test_ccache.*.pyc
%{python3_sitearch}/samba/tests/krb5/__pycache__/test_idmap_nss.*.pyc
%{python3_sitearch}/samba/tests/krb5/__pycache__/test_ldap.*.pyc
%{python3_sitearch}/samba/tests/krb5/__pycache__/test_min_domain_uid.*.pyc
%{python3_sitearch}/samba/tests/krb5/__pycache__/test_rpc.*.pyc
%{python3_sitearch}/samba/tests/krb5/__pycache__/test_smb.*.pyc
%{python3_sitearch}/samba/tests/krb5/__pycache__/xrealm_tests.*.pyc
%{python3_sitearch}/samba/tests/krb5/alias_tests.py
%{python3_sitearch}/samba/tests/krb5/as_canonicalization_tests.py
%{python3_sitearch}/samba/tests/krb5/as_req_tests.py
%{python3_sitearch}/samba/tests/krb5/authn_policy_tests.py
%{python3_sitearch}/samba/tests/krb5/claims_in_pac.py
%{python3_sitearch}/samba/tests/krb5/claims_tests.py
%{python3_sitearch}/samba/tests/krb5/compatability_tests.py
%{python3_sitearch}/samba/tests/krb5/conditional_ace_tests.py
%{python3_sitearch}/samba/tests/krb5/device_tests.py
%{python3_sitearch}/samba/tests/krb5/etype_tests.py
%{python3_sitearch}/samba/tests/krb5/fast_tests.py
%{python3_sitearch}/samba/tests/krb5/gkdi_tests.py
%{python3_sitearch}/samba/tests/krb5/gmsa_tests.py
%{python3_sitearch}/samba/tests/krb5/group_tests.py
%{python3_sitearch}/samba/tests/krb5/kcrypto.py
%{python3_sitearch}/samba/tests/krb5/kdc_base_test.py
%{python3_sitearch}/samba/tests/krb5/kdc_tests.py
%{python3_sitearch}/samba/tests/krb5/kdc_tgs_tests.py
%{python3_sitearch}/samba/tests/krb5/kdc_tgt_tests.py
%{python3_sitearch}/samba/tests/krb5/kpasswd_tests.py
%{python3_sitearch}/samba/tests/krb5/lockout_tests.py
%{python3_sitearch}/samba/tests/krb5/ms_kile_client_principal_lookup_tests.py
%{python3_sitearch}/samba/tests/krb5/netlogon.py
%{python3_sitearch}/samba/tests/krb5/nt_hash_tests.py
%{python3_sitearch}/samba/tests/krb5/pac_align_tests.py
%{python3_sitearch}/samba/tests/krb5/pkinit_tests.py
%{python3_sitearch}/samba/tests/krb5/protected_users_tests.py
%{python3_sitearch}/samba/tests/krb5/raw_testcase.py
%{python3_sitearch}/samba/tests/krb5/rfc4120_constants.py
%{python3_sitearch}/samba/tests/krb5/rfc4120_pyasn1.py
%{python3_sitearch}/samba/tests/krb5/rfc4120_pyasn1_generated.py
%{python3_sitearch}/samba/tests/krb5/rodc_tests.py
%{python3_sitearch}/samba/tests/krb5/salt_tests.py
%{python3_sitearch}/samba/tests/krb5/simple_tests.py
%{python3_sitearch}/samba/tests/krb5/spn_tests.py
%{python3_sitearch}/samba/tests/krb5/test_ccache.py
%{python3_sitearch}/samba/tests/krb5/test_idmap_nss.py
%{python3_sitearch}/samba/tests/krb5/test_ldap.py
%{python3_sitearch}/samba/tests/krb5/test_min_domain_uid.py
%{python3_sitearch}/samba/tests/krb5/test_rpc.py
%{python3_sitearch}/samba/tests/krb5/test_smb.py
%{python3_sitearch}/samba/tests/krb5/s4u_tests.py
%{python3_sitearch}/samba/tests/krb5/xrealm_tests.py
%{python3_sitearch}/samba/tests/krb5_credentials.py
%{python3_sitearch}/samba/tests/ldap_raw.py
%{python3_sitearch}/samba/tests/ldap_referrals.py
%{python3_sitearch}/samba/tests/ldap_spn.py
%{python3_sitearch}/samba/tests/ldap_upn_sam_account.py
%{python3_sitearch}/samba/tests/ldap_whoami.py
%{python3_sitearch}/samba/tests/libsmb.py
%{python3_sitearch}/samba/tests/libsmb-basic.py
%{python3_sitearch}/samba/tests/loadparm.py
%{python3_sitearch}/samba/tests/logfiles.py
%{python3_sitearch}/samba/tests/lsa_string.py
%{python3_sitearch}/samba/tests/messaging.py
%dir %{python3_sitearch}/samba/tests/ndr
%{python3_sitearch}/samba/tests/ndr/gkdi.py
%{python3_sitearch}/samba/tests/ndr/gmsa.py
%{python3_sitearch}/samba/tests/ndr/sd.py
%dir %{python3_sitearch}/samba/tests/ndr/__pycache__
%{python3_sitearch}/samba/tests/ndr/__pycache__/gkdi.*.pyc
%{python3_sitearch}/samba/tests/ndr/__pycache__/gmsa.*.pyc
%{python3_sitearch}/samba/tests/ndr/__pycache__/sd.*.pyc
%{python3_sitearch}/samba/tests/ndr/__pycache__/wbint.*.pyc
%{python3_sitearch}/samba/tests/ndr/wbint.py
%{python3_sitearch}/samba/tests/netbios.py
%{python3_sitearch}/samba/tests/netcmd.py
%{python3_sitearch}/samba/tests/net_join_no_spnego.py
%{python3_sitearch}/samba/tests/net_join.py
%{python3_sitearch}/samba/tests/netlogonsvc.py
%{python3_sitearch}/samba/tests/ntacls.py
%{python3_sitearch}/samba/tests/ntacls_backup.py
%{python3_sitearch}/samba/tests/ntlmdisabled.py
%{python3_sitearch}/samba/tests/ntlm_auth.py
%{python3_sitearch}/samba/tests/ntlm_auth_base.py
%{python3_sitearch}/samba/tests/ntlm_auth_krb5.py
%{python3_sitearch}/samba/tests/pam_winbind.py
%{python3_sitearch}/samba/tests/pam_winbind_chauthtok.py
%{python3_sitearch}/samba/tests/pam_winbind_setcred.py
%{python3_sitearch}/samba/tests/pam_winbind_warn_pwd_expire.py
%{python3_sitearch}/samba/tests/param.py
%{python3_sitearch}/samba/tests/password_hash.py
%{python3_sitearch}/samba/tests/password_hash_fl2003.py
%{python3_sitearch}/samba/tests/password_hash_fl2008.py
%{python3_sitearch}/samba/tests/password_hash_gpgme.py
%{python3_sitearch}/samba/tests/password_hash_ldap.py
%{python3_sitearch}/samba/tests/password_quality.py
%{python3_sitearch}/samba/tests/password_test.py
%{python3_sitearch}/samba/tests/policy.py
%{python3_sitearch}/samba/tests/posixacl.py
%{python3_sitearch}/samba/tests/prefork_restart.py
%{python3_sitearch}/samba/tests/process_limits.py
%{python3_sitearch}/samba/tests/provision.py
%{python3_sitearch}/samba/tests/pso.py
%{python3_sitearch}/samba/tests/py_credentials.py
%{python3_sitearch}/samba/tests/registry.py
%{python3_sitearch}/samba/tests/reparsepoints.py
%{python3_sitearch}/samba/tests/rust.py
%{python3_sitearch}/samba/tests/s3idmapdb.py
%{python3_sitearch}/samba/tests/s3param.py
%{python3_sitearch}/samba/tests/s3passdb.py
%{python3_sitearch}/samba/tests/s3registry.py
%{python3_sitearch}/samba/tests/s3windb.py
%{python3_sitearch}/samba/tests/s3_net_join.py
%{python3_sitearch}/samba/tests/safe_tarfile.py
%{python3_sitearch}/samba/tests/samba3sam.py
%{python3_sitearch}/samba/tests/samba_startup_fl_change.py
%{python3_sitearch}/samba/tests/samba_upgradedns_lmdb.py
%dir %{python3_sitearch}/samba/tests/samba_tool
%{python3_sitearch}/samba/tests/samba_tool/__init__.py
%dir %{python3_sitearch}/samba/tests/samba_tool/__pycache__
%{python3_sitearch}/samba/tests/samba_tool/__pycache__/__init__.*.pyc
%{python3_sitearch}/samba/tests/samba_tool/__pycache__/base.*.pyc
%{python3_sitearch}/samba/tests/samba_tool/__pycache__/computer.*.pyc
%{python3_sitearch}/samba/tests/samba_tool/__pycache__/contact.*.pyc
%{python3_sitearch}/samba/tests/samba_tool/__pycache__/demote.*.pyc
%{python3_sitearch}/samba/tests/samba_tool/__pycache__/dnscmd.*.pyc
%{python3_sitearch}/samba/tests/samba_tool/__pycache__/domain_auth_policy.*.pyc
%{python3_sitearch}/samba/tests/samba_tool/__pycache__/domain_auth_silo.*.pyc
%{python3_sitearch}/samba/tests/samba_tool/__pycache__/domain_claim.*.pyc
%{python3_sitearch}/samba/tests/samba_tool/__pycache__/domain_kds_root_key.*.pyc
%{python3_sitearch}/samba/tests/samba_tool/__pycache__/domain_models.*.pyc
%{python3_sitearch}/samba/tests/samba_tool/__pycache__/drs_clone_dc_data_lmdb_size.*.pyc
%{python3_sitearch}/samba/tests/samba_tool/__pycache__/dsacl.*.pyc
%{python3_sitearch}/samba/tests/samba_tool/__pycache__/forest.*.pyc
%{python3_sitearch}/samba/tests/samba_tool/__pycache__/fsmo.*.pyc
%{python3_sitearch}/samba/tests/samba_tool/__pycache__/gpo.*.pyc
%{python3_sitearch}/samba/tests/samba_tool/__pycache__/gpo_exts.*.pyc
%{python3_sitearch}/samba/tests/samba_tool/__pycache__/group.*.pyc
%{python3_sitearch}/samba/tests/samba_tool/__pycache__/help.*.pyc
%{python3_sitearch}/samba/tests/samba_tool/__pycache__/join.*.pyc
%{python3_sitearch}/samba/tests/samba_tool/__pycache__/join_lmdb_size.*.pyc
%{python3_sitearch}/samba/tests/samba_tool/__pycache__/join_member.*.pyc
%{python3_sitearch}/samba/tests/samba_tool/__pycache__/ntacl.*.pyc
%{python3_sitearch}/samba/tests/samba_tool/__pycache__/ou.*.pyc
%{python3_sitearch}/samba/tests/samba_tool/__pycache__/passwordsettings.*.pyc
%{python3_sitearch}/samba/tests/samba_tool/__pycache__/processes.*.pyc
%{python3_sitearch}/samba/tests/samba_tool/__pycache__/promote_dc_lmdb_size.*.pyc
%{python3_sitearch}/samba/tests/samba_tool/__pycache__/provision_lmdb_size.*.pyc
%{python3_sitearch}/samba/tests/samba_tool/__pycache__/provision_password_check.*.pyc
%{python3_sitearch}/samba/tests/samba_tool/__pycache__/provision_userPassword_crypt.*.pyc
%{python3_sitearch}/samba/tests/samba_tool/__pycache__/rodc.*.pyc
%{python3_sitearch}/samba/tests/samba_tool/__pycache__/schema.*.pyc
%{python3_sitearch}/samba/tests/samba_tool/__pycache__/service_account.*.pyc
%{python3_sitearch}/samba/tests/samba_tool/__pycache__/silo_base.*.pyc
%{python3_sitearch}/samba/tests/samba_tool/__pycache__/sites.*.pyc
%{python3_sitearch}/samba/tests/samba_tool/__pycache__/timecmd.*.pyc
%{python3_sitearch}/samba/tests/samba_tool/__pycache__/user.*.pyc
%{python3_sitearch}/samba/tests/samba_tool/__pycache__/user_auth_policy.*.pyc
%{python3_sitearch}/samba/tests/samba_tool/__pycache__/user_auth_silo.*.pyc
%{python3_sitearch}/samba/tests/samba_tool/__pycache__/user_check_password_script.*.pyc
%{python3_sitearch}/samba/tests/samba_tool/__pycache__/user_get_kerberos_ticket.*.pyc
%{python3_sitearch}/samba/tests/samba_tool/__pycache__/user_getpassword_gmsa.*.pyc
%{python3_sitearch}/samba/tests/samba_tool/__pycache__/user_virtualCryptSHA.*.pyc
%{python3_sitearch}/samba/tests/samba_tool/__pycache__/user_virtualCryptSHA_base.*.pyc
%{python3_sitearch}/samba/tests/samba_tool/__pycache__/user_virtualCryptSHA_gpg.*.pyc
%{python3_sitearch}/samba/tests/samba_tool/__pycache__/user_virtualCryptSHA_userPassword.*.pyc
%{python3_sitearch}/samba/tests/samba_tool/__pycache__/user_wdigest.*.pyc
%{python3_sitearch}/samba/tests/samba_tool/__pycache__/visualize.*.pyc
%{python3_sitearch}/samba/tests/samba_tool/__pycache__/visualize_drs.*.pyc
%{python3_sitearch}/samba/tests/samba_tool/base.py
%{python3_sitearch}/samba/tests/samba_tool/computer.py
%{python3_sitearch}/samba/tests/samba_tool/contact.py
%{python3_sitearch}/samba/tests/samba_tool/demote.py
%{python3_sitearch}/samba/tests/samba_tool/dnscmd.py
%{python3_sitearch}/samba/tests/samba_tool/domain_auth_policy.py
%{python3_sitearch}/samba/tests/samba_tool/domain_auth_silo.py
%{python3_sitearch}/samba/tests/samba_tool/domain_claim.py
%{python3_sitearch}/samba/tests/samba_tool/domain_kds_root_key.py
%{python3_sitearch}/samba/tests/samba_tool/domain_models.py
%{python3_sitearch}/samba/tests/samba_tool/drs_clone_dc_data_lmdb_size.py
%{python3_sitearch}/samba/tests/samba_tool/dsacl.py
%{python3_sitearch}/samba/tests/samba_tool/forest.py
%{python3_sitearch}/samba/tests/samba_tool/fsmo.py
%{python3_sitearch}/samba/tests/samba_tool/gpo.py
%{python3_sitearch}/samba/tests/samba_tool/gpo_exts.py
%{python3_sitearch}/samba/tests/samba_tool/group.py
%{python3_sitearch}/samba/tests/samba_tool/help.py
%{python3_sitearch}/samba/tests/samba_tool/join.py
%{python3_sitearch}/samba/tests/samba_tool/join_lmdb_size.py
%{python3_sitearch}/samba/tests/samba_tool/join_member.py
%{python3_sitearch}/samba/tests/samba_tool/ntacl.py
%{python3_sitearch}/samba/tests/samba_tool/ou.py
%{python3_sitearch}/samba/tests/samba_tool/passwordsettings.py
%{python3_sitearch}/samba/tests/samba_tool/processes.py
%{python3_sitearch}/samba/tests/samba_tool/promote_dc_lmdb_size.py
%{python3_sitearch}/samba/tests/samba_tool/provision_lmdb_size.py
%{python3_sitearch}/samba/tests/samba_tool/provision_password_check.py
%{python3_sitearch}/samba/tests/samba_tool/provision_userPassword_crypt.py
%{python3_sitearch}/samba/tests/samba_tool/rodc.py
%{python3_sitearch}/samba/tests/samba_tool/schema.py
%{python3_sitearch}/samba/tests/samba_tool/service_account.py
%{python3_sitearch}/samba/tests/samba_tool/silo_base.py
%{python3_sitearch}/samba/tests/samba_tool/sites.py
%{python3_sitearch}/samba/tests/samba_tool/timecmd.py
%{python3_sitearch}/samba/tests/samba_tool/user.py
%{python3_sitearch}/samba/tests/samba_tool/user_auth_policy.py
%{python3_sitearch}/samba/tests/samba_tool/user_auth_silo.py
%{python3_sitearch}/samba/tests/samba_tool/user_check_password_script.py
%{python3_sitearch}/samba/tests/samba_tool/user_get_kerberos_ticket.py
%{python3_sitearch}/samba/tests/samba_tool/user_getpassword_gmsa.py
%{python3_sitearch}/samba/tests/samba_tool/user_virtualCryptSHA.py
%{python3_sitearch}/samba/tests/samba_tool/user_virtualCryptSHA_base.py
%{python3_sitearch}/samba/tests/samba_tool/user_virtualCryptSHA_gpg.py
%{python3_sitearch}/samba/tests/samba_tool/user_virtualCryptSHA_userPassword.py
%{python3_sitearch}/samba/tests/samba_tool/user_wdigest.py
%{python3_sitearch}/samba/tests/samba_tool/visualize.py
%{python3_sitearch}/samba/tests/samba_tool/visualize_drs.py
%{python3_sitearch}/samba/tests/samdb.py
%{python3_sitearch}/samba/tests/samdb_api.py
%{python3_sitearch}/samba/tests/sddl.py
%{python3_sitearch}/samba/tests/sddl_conditional_ace.py
%{python3_sitearch}/samba/tests/security.py
%{python3_sitearch}/samba/tests/security_descriptors.py
%{python3_sitearch}/samba/tests/segfault.py
%{python3_sitearch}/samba/tests/sid_strings.py
%{python3_sitearch}/samba/tests/smb.py
%{python3_sitearch}/samba/tests/smb1posix.py
%{python3_sitearch}/samba/tests/smb2symlink.py
%{python3_sitearch}/samba/tests/smb3unix.py
%{python3_sitearch}/samba/tests/smbconf.py
%{python3_sitearch}/samba/tests/smb-notify.py
%{python3_sitearch}/samba/tests/smbd_base.py
%{python3_sitearch}/samba/tests/smbd_fuzztest.py
%{python3_sitearch}/samba/tests/source.py
%{python3_sitearch}/samba/tests/source_chars.py
%{python3_sitearch}/samba/tests/strings.py
%{python3_sitearch}/samba/tests/subunitrun.py
%{python3_sitearch}/samba/tests/tdb_util.py
%{python3_sitearch}/samba/tests/token_factory.py
%{python3_sitearch}/samba/tests/upgrade.py
%{python3_sitearch}/samba/tests/upgradeprovision.py
%{python3_sitearch}/samba/tests/upgradeprovisionneeddc.py
%{python3_sitearch}/samba/tests/usage.py
%{python3_sitearch}/samba/tests/xattr.py

%files test
%{_bindir}/gentest
%{_bindir}/locktest
%{_bindir}/masktest
%{_bindir}/ndrdump
%{_bindir}/smbtorture
%config(noreplace) /etc/ld.so.conf.d/*

%if %with_dc
%{_libdir}/samba/libdlz-bind9-for-torture-private-samba.so
%endif
%{_libdir}/samba/libdsdb-module-private-samba.so

%files usershares
%config(noreplace) %{_sysconfdir}/samba/usershares.conf
%attr(1770,root,usershares) %dir /var/lib/samba/usershares
%{_sysusersdir}/samba-usershares.conf

### WINBIND
%files winbind
%config(noreplace) /etc/ld.so.conf.d/*
%{_libdir}/samba/idmap
%{_libdir}/samba/nss_info
%{_libdir}/samba/libnss-info-private-samba.so
%{_libdir}/samba/libidmap-private-samba.so
%{_sbindir}/winbindd
%{_sysusersdir}/samba-winbind.conf
%attr(750,root,wbpriv) %dir /var/lib/samba/winbindd_privileged
%{_unitdir}/winbind.service
%{_prefix}/lib/NetworkManager

%files winbind-clients
%config(noreplace) /etc/ld.so.conf.d/*
%{_bindir}/ntlm_auth
%{_bindir}/wbinfo
%{_libdir}/samba/krb5/winbind_krb5_localauth.so

%files winbind-krb5-locator
%ghost %{_libdir}/krb5/plugins/libkrb5/winbind_krb5_locator.so
%dir %{_libdir}/samba/krb5
%{_libdir}/samba/krb5/winbind_krb5_locator.so
%{_libdir}/samba/krb5/async_dns_krb5_locator.so
%config(noreplace) /etc/ld.so.conf.d/*

%files winbind-modules
%config(noreplace) /etc/ld.so.conf.d/*
%{_libdir}/libnss_winbind.so*
%{_libdir}/libnss_wins.so*
%{_libdir}/security/pam_winbind.so
%config(noreplace) %{_sysconfdir}/security/pam_winbind.conf

%if %with_clustering_support
%files -n ctdb
%doc ctdb/README
%doc ctdb/doc/examples
# Obsolete
%config(noreplace, missingok) %{_sysconfdir}/sysconfig/ctdb

%dir %{_sysconfdir}/ctdb
%config(noreplace) %{_sysconfdir}/ctdb/ctdb.conf
%config(noreplace) %{_sysconfdir}/ctdb/notify.sh
%config(noreplace) %{_sysconfdir}/ctdb/debug-hung-script.sh
%config(noreplace) %{_sysconfdir}/ctdb/ctdb-backup-persistent-tdbs.sh
%config(noreplace) %{_sysconfdir}/ctdb/ctdb-crash-cleanup.sh
%config(noreplace) %{_sysconfdir}/ctdb/debug_locks.sh

%{_sysconfdir}/ctdb/functions
%{_sysconfdir}/ctdb/nfs-linux-kernel-callout
%ghost %{_sysconfdir}/ctdb/statd-callout

%dir %{_sysconfdir}/ctdb/events
%dir %{_sysconfdir}/ctdb/events/legacy
%dir %{_sysconfdir}/ctdb/events/notification
%{_sysconfdir}/ctdb/events/notification/README

%dir %{_sysconfdir}/ctdb/nfs-checks.d
%{_sysconfdir}/ctdb/nfs-checks.d/README
%config(noreplace) %{_sysconfdir}/ctdb/nfs-checks.d/00.portmapper.check
%config(noreplace) %{_sysconfdir}/ctdb/nfs-checks.d/10.status.check
%config(noreplace) %{_sysconfdir}/ctdb/nfs-checks.d/20.nfs.check
%config(noreplace) %{_sysconfdir}/ctdb/nfs-checks.d/30.nlockmgr.check
%config(noreplace) %{_sysconfdir}/ctdb/nfs-checks.d/40.mountd.check
%config(noreplace) %{_sysconfdir}/ctdb/nfs-checks.d/50.rquotad.check

%{_sbindir}/ctdbd
%{_bindir}/ctdb
%{_bindir}/ctdb_diagnostics
%{_bindir}/ltdbtool
%{_bindir}/onnode
%{_bindir}/ping_pong

%dir %{_libexecdir}/ctdb
%{_libexecdir}/ctdb/ctdb-config
%{_libexecdir}/ctdb/ctdb-event
%{_libexecdir}/ctdb/ctdb-eventd
%{_libexecdir}/ctdb/ctdb_killtcp
%{_libexecdir}/ctdb/ctdb_lock_helper
%{_libexecdir}/ctdb/ctdb_lvs
%{_libexecdir}/ctdb/ctdb_mutex_fcntl_helper
%{_libexecdir}/ctdb/ctdb_natgw
%{_libexecdir}/ctdb/ctdb-path
%{_libexecdir}/ctdb/ctdb_recovery_helper
%{_libexecdir}/ctdb/ctdb_smnotify_helper
%{_libexecdir}/ctdb/ctdb_takeover_helper
%{_libexecdir}/ctdb/statd_callout
%{_libexecdir}/ctdb/statd_callout_helper
%{_libexecdir}/ctdb/tdb_mutex_check

%dir %{_localstatedir}/lib/ctdb/
%dir %{_localstatedir}/lib/ctdb/persistent
%dir %{_localstatedir}/lib/ctdb/state
%dir %{_localstatedir}/lib/ctdb/volatile

%ghost %dir /run/ctdb
%{_tmpfilesdir}/ctdb.conf

%{_unitdir}/ctdb.service

%dir %{_datadir}/ctdb
%dir %{_datadir}/ctdb/events
%dir %{_datadir}/ctdb/events/legacy/
%{_datadir}/ctdb/events/legacy/00.ctdb.script
%{_datadir}/ctdb/events/legacy/01.reclock.script
%{_datadir}/ctdb/events/legacy/05.system.script
%{_datadir}/ctdb/events/legacy/10.interface.script
%{_datadir}/ctdb/events/legacy/11.natgw.script
%{_datadir}/ctdb/events/legacy/11.routing.script
%{_datadir}/ctdb/events/legacy/13.per_ip_routing.script
%{_datadir}/ctdb/events/legacy/20.multipathd.script
%{_datadir}/ctdb/events/legacy/31.clamd.script
%{_datadir}/ctdb/events/legacy/40.vsftpd.script
%{_datadir}/ctdb/events/legacy/41.httpd.script
%{_datadir}/ctdb/events/legacy/46.update-keytabs.script
%{_datadir}/ctdb/events/legacy/47.samba-dcerpcd.script
%{_datadir}/ctdb/events/legacy/48.netbios.script
%{_datadir}/ctdb/events/legacy/49.winbind.script
%{_datadir}/ctdb/events/legacy/50.samba.script
%{_datadir}/ctdb/events/legacy/60.nfs.script
%{_datadir}/ctdb/events/legacy/70.iscsi.script
%{_datadir}/ctdb/events/legacy/91.lvs.script
%{_datadir}/ctdb/events/legacy/95.database.script
%dir %{_datadir}/ctdb/scripts
%{_datadir}/ctdb/scripts/winbind_ctdb_updatekeytab.sh
%config(noreplace) /etc/ld.so.conf.d/*

#endif with_clustering_support
%endif

%files help
%{_mandir}/man*
%dir %{_datadir}/samba/admx
%{_datadir}/samba/admx/GNOME_Settings.admx
%{_datadir}/samba/admx/samba.admx
%dir %{_datadir}/samba/admx/en-US
%{_datadir}/samba/admx/en-US/GNOME_Settings.adml
%{_datadir}/samba/admx/en-US/samba.adml
%dir %{_datadir}/samba/admx/ru-RU
%{_datadir}/samba/admx/ru-RU/GNOME_Settings.adml

%if %{with_winexe}
### WINEXE
%files winexe
%{_bindir}/winexe
%endif

%files -n libldb
%license lib/ldb/LICENSE
%{_libdir}/libldb.so.*
%{_bindir}/ldbadd
%{_bindir}/ldbdel
%{_bindir}/ldbedit
%{_bindir}/ldbmodify
%{_bindir}/ldbrename
%{_bindir}/ldbsearch
%dir %{_libdir}/samba
%{_libdir}/samba/libldb-key-value-private-samba.so
%{_libdir}/samba/libldb-tdb-err-map-private-samba.so
%{_libdir}/samba/libldb-tdb-int-private-samba.so
%{_libdir}/samba/libldb-mdb-int-private-samba.so
%{_libdir}/samba/libldb-cmdline-private-samba.so
%dir %{_libdir}/samba/ldb
%{_libdir}/samba/ldb/asq.so
%{_libdir}/samba/ldb/ldb.so
%{_libdir}/samba/ldb/mdb.so
%{_libdir}/samba/ldb/paged_searches.so
%{_libdir}/samba/ldb/rdn_name.so
%{_libdir}/samba/ldb/sample.so
%{_libdir}/samba/ldb/server_sort.so
%{_libdir}/samba/ldb/skel.so
%{_libdir}/samba/ldb/tdb.so

%files -n libldb-devel
%{_includedir}/samba-4.0/ldb_module.h
%{_includedir}/samba-4.0/ldb_handlers.h
%{_includedir}/samba-4.0/ldb_errors.h
%{_includedir}/samba-4.0/ldb_version.h
%{_includedir}/samba-4.0/ldb.h
%{_libdir}/libldb.so

%{_libdir}/pkgconfig/ldb.pc

%files -n python3-ldb
%{python3_sitearch}/ldb.cpython-*.so
%{_libdir}/samba/libpyldb-util.cpython-*-private-samba.so
%{python3_sitearch}/_ldb_text.py
%{python3_sitearch}/__pycache__/_ldb_text.cpython-*.py*

%files -n libldb-help
%{_mandir}/man1/ldbadd.1.*
%{_mandir}/man1/ldbdel.1.*
%{_mandir}/man1/ldbedit.1.*
%{_mandir}/man1/ldbmodify.1.*
%{_mandir}/man1/ldbrename.1.*
%{_mandir}/man1/ldbsearch.1.*
%{_mandir}/man3/ldb*.gz
%{_mandir}/man3/ldif*.gz

%changelog
* Wed Mar 12 2025 yanglu <yanglu72@h-partners.com> - 4.22.0-1
- Type:requirement
- ID:NA
- SUG:NA
- DESC:update samba to 4.22.0

* Tue Sep 24 2024 xinghe <xinghe2@h-partners.com> - 4.19.3-6
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:add Obsoletes '< version' in spec

* Thu May 30 2024 zhangyaqi <zhangyaqi@kylinos.cn> - 4.19.3-5
- Type:bugfis
- ID:NA
- SUG:NA
- DESC:s4:dsdb:strcasecmp_with_ldb_val() avoids overflow

* Thu Feb 1 2024 jiahua.yu <jiahua.yu@shingroup.cn> - 4.19.3-4
- Type:feature
- ID:NA
- SUG:NA
- DESC:init support for arch ppc64le

* Fri Jan 19 2024 xinghe <xinghe2@h-partners.com> - 4.19.3-3
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:speeds up the creation of source rpm

* Tue Jan 16 2024 xinghe <xinghe2@h-partners.com> - 4.19.3-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:remove sensitive info

* Thu Dec 28 2023 xinghe <xinghe2@h-partners.com> - 4.19.3-1
- Type:requirement
- ID:NA
- SUG:NA
- DESC:update samba to 4.19.3

* Thu Oct 12 2023 xinghe <xinghe2@h-partners.com> - 4.18.5-2
- Type:cves
- ID:CVE-2023-3961 CVE-2023-4091 CVE-2023-4154 CVE-2023-42669 CVE-2023-42670
- SUG:NA
- DESC:fix CVE-2023-3961 CVE-2023-4091 CVE-2023-4154 CVE-2023-42669 CVE-2023-42670

* Mon Jul 24 2023 gaihuiying <eaglegai@163.com> - 4.18.5-1
- Type:requirement
- ID:NA
- SUG:NA
- DESC:update samba to 4.18.5

* Tue Jun 27 2023 yanglu <yanglu72@h-partners.com> - 4.17.5-6
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:split samba-client-libs and samba-dc-libs

* Wed Jun 21 2023 yanglu <yanglu72@h-partners.com> - 4.17.5-5
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:add Obsoletes '< version' and Provides '= version' in spec

* Sat Apr 01 2023 xinghe <xinghe2@h-partners.com> - 4.17.5-4
- Type:cves
- ID:CVE-2023-0225 CVE-2023-0614 CVE-2023-0922
- SUG:NA
- DESC:fix CVE-2023-0225 CVE-2023-0614 CVE-2023-0922

* Tue Mar 7 2023 doupengda <doupengda@loongson.cn> - 4.17.5-3
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:add ld.bfd for loongarch

* Wed Feb 08 2023 laokz <zhangkai@iscas.ac.cn> - 4.17.5-2
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:isolate unsupported gold for riscv64

* Wed Feb 01 2023 gaihuiying <eaglegai@163.com> - 4.17.5-1
- Type:requirement
- ID:NA
- SUG:NA
- DESC:update samba to 4.17.5-1

* Thu Dec 29 2022 xinghe <xinghe2@h-partners.com> - 4.17.2-5
- Type:cves
- ID:CVE-2022-38023 CVE-2022-37966 CVE-2022-37967
- SUG:NA
- DESC:fix CVE-2022-38023 CVE-2022-37966 CVE-2022-37967

* Sat Dec 17 2022 xinghe <xinghe2@h-partners.com> - 4.17.2-4
- Type:cves
- ID:CVE-2022-44640
- SUG:NA
- DESC:fix CVE-2022-44640

* Thu Dec 08 2022 xinghe <xinghe2@h-partners.com> - 4.17.2-3
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:remove runpath of samba's binary files

* Mon Nov 21 2022 zhouyihang <zhouyihang3@h-partners.com> - 4.17.2-2
- Type:cves
- ID:CVE-2022-42898
- SUG:NA
- DESC:fix CVE-2022-42898

* Tue Nov 08 2022 xinghe <xinghe2@h-partners.com> - 4.17.2-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC: update to 4.17.2

* Wed Oct 26 2022 xinghe <xinghe2@h-partners.com> - 4.15.3-12
- Type:cves
- ID:CVE-2022-3437
- SUG:NA
- DESC:fix CVE-2022-3437

* Thu Sep 29 2022 xinghe <xinghe2@h-partners.com> - 4.15.3-11
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:clear the compilation alarms

* Wed Aug 31 2022 zhouyihang <zhouyihang3@h-partners.com> - 4.15.3-10
- Type:cves
- ID:CVE-2022-1615
- SUG:NA
- DESC:fix CVE-2022-1615

* Fri Aug 26 2022 zhouyihang <zhouyihang3@h-partners.com> - 4.15.3-9
- Type:cves
- ID:CVE-2022-32743
- SUG:NA
- DESC:fix CVE-2022-32743

* Fri Aug 12 2022 xinghe <xinghe2@h-partners.com> - 4.15.3-8
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:add ldb require version

* Tue Aug 02 2022 xinghe <xinghe2@h-partners.com> - 4.15.3-7
- Type:cves
- ID:CVE-2022-32746 CVE-2022-2031 CVE-2022-32744 CVE-2022-32742 CVE-2022-32745
- SUG:NA
- DESC:fix CVE-2022-32746 CVE-2022-2031 CVE-2022-32744 CVE-2022-32742 CVE-2022-32745

* Wed Jul 20 2022 gaihuiying <eaglegai@163.com> - 4.15.3-6
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:add macros to control if need cups in configure file

* Thu Jul 14 2022 gaihuiying <eaglegai@163.com> - 4.15.3-5
- Type:cves
- ID:CVE-2021-44141
- SUG:NA
- DESC:fix CVE-2021-44141

* Fri Mar 11 2022 xingwei <xingwei14@h-partners.com> - 4.15.3-4
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:remove runpath of samba's binary files

* Tue Feb 15 2022 gaihuiying <eaglegai@163.com> - 4.15.3-3
- Type:cves
- ID:CVE-2022-0336 CVE-2021-44142
- SUG:NA
- DESC:backport to fix CVE-2022-0336 CVE-2021-44142

* Tue Jan 18 2022 zengwefeng<zwfeng@huawei.com> - 4.15.3-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:add "openEuler" macro to control if cups is needed

* Tue Jan 18 2022 zengwefeng<zwfeng@huawei.com> - 4.15.3-1
- Type:requirement
- ID:NA
- SUG:NA
- DESC:update samba to 4.15.3

* Mon Oct 25 2021 gaihuiying <gaihuiying1@huawei.com> - 4.12.5-7
- Type:cves
- ID:CVE-2021-3671
- SUG:NA
- DESC:fix CVE-2021-3671

* Fri Sep 10 2021 gaihuiying <gaihuiying1@huawei.com> - 4.12.5-6
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:remove runpath of samba's binary files

* Mon May 31 2021 gaihuiying <gaihuiying1@huawei.com> - 4.12.5-5
- Type:cves
- ID:CVE-2020-27840 CVE-2021-20277 CVE-2021-20254
- SUG:NA
- DESC:fix CVE-2020-27840 CVE-2021-20277 CVE-2021-20254

* Thu May 27 2021 lijingyuan <lijingyuan3@huawei.com> - 4.12.5-4
- Type:bugfix
- Id:
- SUG:NA
- DESC:Add the compilation dependency of gcc.

* Sat Mar 27 2021 seuzw <930zhaowei@163.com> - 4.12.5-3
- Type:cves
- Id:CVE-2020-14318 CVE-2020-14323 CVE-2020-14383
- SUG:NA
- DESC:fix CVE-2020-14318 CVE-2020-14323 CVE-2020-14383

* Fri Sep 25 2020 liulong <liulong20@huawei.com> - 4.12.5-2
- Type:cves
- Id:CVE-2020-1472
- SUG:NA
- DESC:fix CVE-2020-1472

* Thu Jul 30 2020 yuboyun <yuboyun@huawei.com> - 4.12.5-1
- Type:NA
- Id:NA
- SUG:NA
- DESC:update to 4.12.5

* Fri May 29 2020 songzifeng <songzifeng1@huawei.com> - 4.11.6-7
- fix the conflict of man and help

* Wed May 20 2020 zhouyihang <zhouyihang3@huawei.com> - 4.11.6-6
- fix CVE-2020-10700,CVE-2020-10704

* Sat Mar 21 2020 songnannan <songnannan2@huawei.com> - 4.11.6-5
- bugfix about update

* Sat Mar 21 2020 songnannan <songnannan2@huawei.com> - 4.11.6-4
- bugfix about update

* Mon Feb 24 2020 hexiujun <hexiujun1@huawei.com> - 4.11.6-3
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:unpack libs subpackage

* Fri Feb 21 2020 openEuler Buildteam <buildteam@openeuler.org> - 4.11.6-2
- use zcat instead of xzcat

* Mon Feb 10 2020 openEuler Buildteam <buildteam@openeuler.org> - 4.11.6-1
- Update to Samba 4.11.6

* Fri Jan 10 2020 openEuler Buildteam <buildteam@openeuler.org> - 4.9.8-9
- clean unused file

* Tue Dec 31 2019 openEuler Buildteam <buildteam@openeuler.org> - 4.9.1-8
- Type:NA
- Id:NA
- SUG:NA
- DESC:update tarball

* Sat Dec 21 2019 openEuler Buildteam <buildteam@openeuler.org> - 4.9.1-7
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:fix CVE

* Thu Dec 19 2019 openEuler Buildteam <buildteam@openeuler.org> - 4.9.1-6
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:modify the changelog message

* Tue Dec 3 2019 openEuler Buildteam <buildteam@openeuler.org> - 4.9.1-5
- Type: NA
- ID:   NA
- SUG:  NA
- DESC: optimize the spec file

* Thu Nov 21 2019 openEuler Buildteam <buildteam@openeuler.org> - 4.9.1-4
- Type: enhancement
- ID:   NA
- SUG:  NA
- DESC:modify spec file

* Mon Sep 23 2019 huzhiyu<huzhiyu1@huawei.com> - 4.9.1-3
- Package init
